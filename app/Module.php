<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table='module';
    protected $primaryKey='moduleid';
    public $timestamps=false;


    public function users()
    {
    	return $this->belongsToMany('App\User','moduleuser','id','moduleid');
    }
}
