<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLevelUser extends Model
{
     protected $primaryKey='accessleveluserid';
    protected $table='accessleveluser';
    public $timestamps=false;
}
