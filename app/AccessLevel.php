<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    protected $table='accesslevel';
    protected $primaryKey='accesslevelid';
    public $timestamps=false;

    public function users()
        {

    return $this->belongsToMany('App\User','accessleveluser','id','accesslevelid');

        }
}
