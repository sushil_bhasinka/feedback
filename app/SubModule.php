<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubModule extends Model
{
    protected $primaryKey='submoduleid';
    protected $table='submodule';
    public $timestamps=false;

    public function users()
    {
    	return $this->belongsToMany('App\User','submoduleuser','id','submoduleid');
    }
}
