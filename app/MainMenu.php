<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{
    protected $table = 'mainmenus';
    protected $primaryKey = 'mainmenuid';
    public $timestamps = true;
}
