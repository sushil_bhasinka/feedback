<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    protected $primaryKey='usertypeid';
    protected $table='usertype';
    public $timestamps=false;
}
