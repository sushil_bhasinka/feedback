<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use DB;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view)
        {
        if(Auth::user()){
            $modules = DB::table('module')
            ->join('moduleuser','moduleuser.moduleid','=','module.moduleid')
            ->where('moduleuser.id','=',Auth::user()->id)->whereIn('module.usertypeid',[2,4])->orderby('moduleorder')->get();
            $submodules = DB::table('submodule')
            ->join('submoduleuser','submoduleuser.submoduleid','=','submodule.submoduleid')
            ->where('submoduleuser.id','=',Auth::user()->id)->orderby('submoduleorder')->get();
            $view->with('modules', $modules);
            $view->with('submodules', $submodules);

        }});
    }
}
