<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{

    

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function modules()
    {
        return $this->belongsToMany('App\Module','moduleuser','id','moduleid');

    }
    
    public function Submodules()
    {
        return $this->belongsToMany('App\SubModule','submoduleuser','id','submoduleid');

    }

    public function accesslevels()
    {

        return $this->belongsToMany('App\AccessLevel','accessleveluser','id','accesslevelid');

    }
}
