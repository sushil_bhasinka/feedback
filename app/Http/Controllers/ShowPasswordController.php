<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class ShowPasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $levels=DB::select('select * from levels order by levelid');
        $faculties=DB::select('select * from faculties order by facultyid');
        $courses=DB::select('select * from courses  order by courseid');

        $classes=DB::table('studentclass')
        ->join('levels', 'levels.levelid', '=', 'studentclass.levelid')
        ->select('levels.levelname', 'studentclass.classid', 'studentclass.classname')
        ->orderBy('studentclass.classid', 'ASC')
        ->where([['levels.levelname','School'] ])
        ->get();
        $sections=DB::select('select * from studentsection  order by sectionid');
        
         $students=DB::table('studentmaster')
          ->leftjoin('nationality','nationality.nationalityid', '=', 'studentmaster.nationalityid')
          ->leftjoin('religion','religion.religionid', '=', 'studentmaster.religionid')
          ->leftjoin('caste', 'caste.casteid', '=', 'studentmaster.casteid')
          ->leftjoin('users', 'users.id', '=', 'studentmaster.postedby')
          ->leftjoin('studentstatus', 'studentstatus.studentstatusid', '=', 'studentmaster.studentstatusid')
         ->leftjoin('studentdetail', 'studentdetail.studentmasterid', '=', 'studentmaster.studentmasterid')

         ->leftjoin('courses','courses.courseid', '=', 'studentdetail.courseid')
         ->leftjoin('faculties', 'faculties.facultyid', '=', 'studentdetail.facultyid')
        ->leftjoin('levels', 'levels.levelid', '=', 'studentdetail.levelid')
        ->leftjoin('studentbatch', 'studentbatch.batchid', '=', 'studentdetail.batchid')
        ->leftjoin('studentclass', 'studentclass.classid', '=', 'studentdetail.classid')
        ->leftjoin('studentsection', 'studentsection.sectionid', '=', 'studentdetail.sectionid')
        ->leftjoin('studentshift', 'studentshift.shiftid', '=', 'studentdetail.shiftid')
        ->leftjoin('academicyear', 'academicyear.academicyearid', '=', 'studentdetail.academicyearid')
        ->leftjoin('transporttype', 'transporttype.transporttypeid', '=', 'studentdetail.transporttypeid')
        ->leftjoin('lunchtype', 'lunchtype.lunchtypeid', '=', 'studentdetail.lunchtypeid')
        ->leftjoin('house', 'house.houseid', '=', 'studentdetail.houseid')
         ->leftjoin('studenttype', 'studenttype.studenttypeid', '=', 'studentdetail.studenttypeid')
        
        ->select('studentmaster.studentmasterid', 'studentmaster.fullname','studentmaster.dateofbirthbs', 'studentmaster.dateofbirthad' ,'studentmaster.age','studentmaster.admissiondatebs', 'studentmaster.admissiondatead', 'studentmaster.gender', 'nationality.nationalityname', 'religion.religionname', 'caste.castename', 'studentstatus.studentstatusname', 'studentmaster.registrationno', 'studentmaster.symbolno', 'courses.course', 'faculties.faculty', 'levels.levelname', 'studentbatch.batchname', 'studentclass.classname', 'studentsection.section', 'studentshift.shift', 'transporttype.transporttypename', 'lunchtype.lunchtypename', 'house.housename', 'studenttype.studenttypename', 'academicyear.academicyear','users.name', 'studentmaster.posteddate', 'studentdetail.rollno' )
        ->orderBy('studentmaster.studentmasterid', 'ASC')
         ->where([['levels.levelname', 'School'], ['academicyear.academicyearid', currentacyear()]])
        ->paginate(10);
        return view('showpasswords.index')->withStudents($students)->withlevels($levels)->withFaculties($faculties)->withCourses($courses)->withClasses($classes)->withSection($sections); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
