<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use Auth;
use App\Module;


class ModuleController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allmodules=DB::table('module')->select('moduleid', 'modulename', 'moduleorder','usertypeid')->orderBy('moduleorder', 'ASC')->paginate(10);
        $usertype=DB::select("select * from usertype order by usertypeid");
         return view('modules.create')->withAllmodules($allmodules)->withUsertype($usertype);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $aaa=DB::table('module')
        ->where([
            ['modulename', '=', $request->modulename]
            
        ])  
        ->get();

        $bbb=DB::table('module')
        ->where([
            ['moduleorder', '=', $request->moduleorder]
            
        ])  
        ->get();
        
        if(count($aaa)!=0)
        {
            $this->validate($request, array(
                'modulename'=>'required|max:50|unique:module,modulename'
                ));
        }

        elseif(count($bbb)!=0)
        {
            $this->validate($request, array(
                'moduleorder'=>'required|max:50|unique:module,moduleorder'
                ));
        }

        else
        {
            $this->validate($request, array(
               'modulename'=>'required|max:50',
               'moduleorder'=>'required|max:50',
               'usertypeid'=>'required'
                ));
        }
        $module=new Module;

        $module->moduleid=$request->moduleid;
        $module->modulename=$request->modulename;
        $module->moduleorder=$request->moduleorder;
        $module->usertypeid=$request->usertypeid;

        $module->save();

        Session::flash('success', 'Module has been saved successfully ');

        return redirect()->route('modules.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module=Module::where([['moduleid', $id]])->first();
        $usertype=DB::select("select * from usertype order by usertypeid");
        return view('modules.edit')->withModule($module)->withUsertype($usertype);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module=Module::where([['moduleid', $id]])->first();

         $aaa=DB::table('module')
        ->where([
            ['modulename', '=', $request->modulename]
        ])  
        ->get();

        $bbb=DB::table('module')
        ->where([
            ['moduleorder', '=', $request->moduleorder]
        ])  
        ->get();
        
        if(count($aaa)!=0 && $request->modulename != $module->modulename)
        {
        
        $this->validate($request, array(
            'modulename'=>'required|max:50|unique:module,modulename'
           
        ));
    }

      elseif(count($bbb)!=0 && $request->moduleorder != $module->moduleorder)
        {
        
        $this->validate($request, array(
            'moduleorder'=>'required|max:50|unique:module,moduleorder'
           
        ));
    }
   
   
        //save into database
        
        $module->modulename=$request->modulename;
        $module->moduleorder=$request->moduleorder;
        $module->usertypeid=$request->usertypeid;
        $module->save();

        Session::flash('success', 'Module has been updated successfully.');
        return redirect()->route('modules.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uniquemodulename(Request $request)
    {
        
         $data1=db::table('module')->select('modulename')->where([[DB::raw('upper(modulename)'), $request->modulename]])->get();
       
        $uniquemodule=db::table('module')->select('modulename')->where([[DB::raw('upper(modulename)'), $request->modulename], ['moduleid', $request->moduleid]])->get();
        return response()->json(['data1'=> $data1, 'uniquemodule'=>$uniquemodule]);

    }
}
