<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Module;
use App\SubModule;
use DB;
use Auth;
use App\MainUrl;

class SubModuleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $mod = Module::orderBy('moduleorder', 'ASC')->pluck('modulename', 'moduleid');
       
         $sub=DB::table('module')->join('submodule', 'submodule.moduleid', '=', 'module.moduleid')->select('submodule.submoduleid', 'submodule.submodulename', 'submodule.submoduleorder', 'module.modulename', 'submodule.url')->get();//paginate(10);
         
        return view('submodules.create')->withMod($mod)->withSub($sub);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $aaa=DB::table('submodule')
        ->where([
            ['submodulename', '=', $request->submodulename],
             ['moduleid', '=', $request->moduleid]
            
        ])  
        ->get();

         $ccc=DB::table('submodule')
        ->where([
            ['url', '=', $request->url],
            ['moduleid', '=', $request->moduleid]
            
        ])  
        ->get();
        
        if(count($aaa)!=0)
        {
        
        $this->validate($request, array(
            'submodulename'=>'required|max:50|unique:submodule,submodulename'
            
           
        ));
        }
        elseif(count($ccc)!=0)
        {
         $this->validate($request, array(
            'url'=>'required|max:50|unique:submodule,url'
            
           
        ));
        }

        else
        {
        $this->validate($request, array(
           'submodulename'=>'required|max:50',
           'submoduleorder'=>'required|max:50',
           
           
        ));
        }
        $submodule=new SubModule;

        $submodule->submoduleid=$request->submoduleid;
        $submodule->moduleid=$request->moduleid;
        $submodule->submodulename=$request->submodulename;
        $submodule->submoduleorder=$request->submoduleorder;
        $submodule->url=$request->url;

        $submodule->save();
        Session::flash('success', 'Sub Module has been saved successfully.');

        return redirect()->route('submodules.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $submodule=SubModule::where([['submoduleid', $id]])->first();
        
        $modules=DB::table('module')->get();

        return view('submodules.edit')->withModules($modules)->withSubmodule($submodule);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $submodule=SubModule::where([['submoduleid', $id]])->first();
        //validation form modulename
        if($request->submodulename == $submodule->submodulename)
         {
            $this->validate($request, array(
                'submoduleorder'=>'required'
                         
                          ));
        }
        elseif ($request->url==$submodule->url) {
            $this->validate($request, array(
                'submoduleorder'=>'required'
                         
                          ));
        }
         elseif($request->url!=$submodule->url)
        {
           $validurl=DB::select("select * from submodule where moduleid=". $request->moduleid . "and url= '".$request->url."'");

            if(count($validurl)!=0)
            {
                $this->validate($request, array(
                        
                        'url'=>'required|max:50|unique:submodule,url'));
            }
            else
            {
                $this->validate($request, array(
                        'url'=>'required',

                        ));
            }

        }  
        else
        {
           $validsubmodule=DB::select("select * from submodule where moduleid=". $request->moduleid . "and submodulename= '".$request->submodulename."'");

            if(count($validsubmodule)!=0)
            {
                $this->validate($request, array(
                        'submoduleorder'=>'required',
                        'submodulename'=>'required|max:50|unique:submodule,submodulename'));
            }
            else
            {
                $this->validate($request, array(
                        'submoduleorder'=>'required'
                        ));
            }

        }  
        
        //save into database
        $submodule->moduleid=$request->moduleid;
        $submodule->submodulename=$request->submodulename;
        $submodule->submoduleorder=$request->submoduleorder;
        $submodule->url=$request->url;
        $submodule->save();
        //set flash data with success message
        Session::flash('success', ' This Sub Module has been updated successfully.');
        return redirect()->route('submodules.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findsubmodules(Request $request)
    {
        $data=DB::table('submodule')->select('submoduleid', 'submodulename', 'submoduleorder', 'url')->where([['moduleid', $request->moduleid]])->get();
        return response()->json($data);
    }

    public function uniquesubmodulename(Request $request)
    {

         $data1=db::table('submodule')->select('submodulename')->where([['moduleid', $request->moduleid],[DB::raw('upper(submodulename)'), $request->submodulename]])->get();
       
        $uniquesubmodule=db::table('submodule')->select('submodulename')->where([[DB::raw('upper(submodulename)'), $request->submodulename], ['moduleid', $request->moduleid],['submoduleid', $request->submoduleid]])->get();
        return response()->json(['data1'=> $data1, 'uniquesubmodule'=>$uniquesubmodule]);
    }
}
