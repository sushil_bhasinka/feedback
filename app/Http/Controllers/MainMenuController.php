<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MainMenu;
use Session;
use DB;

class MainMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainmenu = MainMenu::orderBy('mainmenuid', 'desc')->paginate(10);
        return view('mainmenu.index')->withMainmenu($mainmenu);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mainmenu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request,[
            'menuname'=>'required|max:50',
            'menuurl'=>'required|max:255',
        ]);

        //populate data to store from request to object
        $mainmenu = new MainMenu;
        $mainmenu->mainmenuname = $request->menuname;
        $mainmenu->mainmenuurl = $request->menuurl;
        $mainmenu->mainmenuorder = $request->menudisplayorder;
        
        $mainmenu->save();

        //Setup flash message
        Session::flash('success', 'Menu saved successfully.');

        //Return to index
        return redirect()->route('mainmenu.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mainmenu = MainMenu::findorfail($id);
        return view('mainmenu.edit')->withMainmenu($mainmenu);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
