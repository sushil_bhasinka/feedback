<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\FileUploadMaster;
use App\FileUploadDetail;
use App\Category;
use App\User;
use App\OfficeSetup;
use App\FrontEnd;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $feedbacks = FrontEnd::get();
        return view('globaladminhome',compact('feedbacks'));
    }

    public function feedback()
    {
        $feedbacks = FrontEnd::get();
        return view('feedback.feedbackshow',compact('feedbacks'));
    }

    public function confirmation(Request $request)
    {
        FrontEnd::where('feedbackid',$request->feedbackid)->update(['status'=>$request->form_submit]);
        return redirect('/feedback');
    }
}
