<?php

namespace App\Http\Controllers;

use App\FrontEnd;
use Illuminate\Http\Request;
use carbon\carbon;

class FrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbacks = FrontEnd::get();
        return view('frontend.index',compact('feedbacks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feedback = new FrontEnd;
        $feedback->question1=$request->question1;
        $feedback->question2=$request->question2;
        $feedback->question3=$request->question3;
        $feedback->question4=$request->question4;
        $feedback->question5=$request->question5;
        $feedback->question6=$request->question6;
        $feedback->question7=$request->question7;
        $feedback->question8=$request->question8;
        $feedback->question9=$request->question9;
        $feedback->clientname=$request->clientName;
        $feedback->comments=$request->comments;
        $feedback->date=carbon::now()->toDateString();

        $feedback->save();
        return redirect('/#feedback_success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FrontEnd  $frontEnd
     * @return \Illuminate\Http\Response
     */
    public function show(FrontEnd $frontEnd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FrontEnd  $frontEnd
     * @return \Illuminate\Http\Response
     */
    public function edit(FrontEnd $frontEnd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FrontEnd  $frontEnd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FrontEnd $frontEnd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FrontEnd  $frontEnd
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrontEnd $frontEnd)
    {
        //
    }
}
