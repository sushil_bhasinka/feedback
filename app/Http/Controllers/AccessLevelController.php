<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\AccessLevel;
use App\AccessLevelButton;
use Auth;
class AccessLevelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accesslevels=DB::table('accesslevel')->select('accesslevelid', 'accesslevelname')->get();//paginate(5);
        return view('accesslevels.create')->withAccesslevels($accesslevels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aaa=DB::table('accesslevel')
        ->where([
            ['accesslevelname', '=', $request->accesslevelname]
        ])  
        ->get();
        
        if(count($aaa)!=0)
        {
        
        $this->validate($request, array(
            'accesslevelname'=>'required|max:50|unique:accesslevel,accesslevelname'
           
        ));
    }
    else
        {
        
        $this->validate($request, array(
           'accesslevelname'=>'required|max:50'
           
        ));
    }
        $accesslevel=new AccessLevel;
        $accesslevel->accesslevelid=$request->accesslevelid;
        $accesslevel->accesslevelname=$request->accesslevelname;
       

        $accesslevel->save();

        Session::flash('success', 'Access Level has been saved successfully');

        return redirect()->route('accesslevels.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accesslevel=AccessLevel::where([['accesslevelid',$id]])->first();
        return view('accesslevels.edit')->withAccesslevel($accesslevel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $accesslevel=AccessLevel::where([['accesslevelid',$id]])->first();

         $aaa=DB::table('accesslevel')
        ->where([
            ['accesslevelname', '=', $request->accesslevelname]
        ])  
        ->get();
        
        if(count($aaa)!=0 && $request->accesslevelname != $accesslevel->accesslevelname)
        {
        
        $this->validate($request, array(
            'accesslevelname'=>'required|max:50|unique:accesslevel,accesslevelname'
           
        ));
    }
   
        //save into database
       $accesslevel->accesslevelname=$request->accesslevelname;
       
        $accesslevel->save();

        //set flash data with success message
        Session::flash('success', 'Access level has been updated successfully.');

        return redirect()->route('accesslevels.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uniqueaccesslevelname(Request $request)
    {
         $data1=db::table('accesslevel')->select('accesslevelname')->where([[DB::raw('upper(accesslevelname)'), $request->accesslevelname]])->get();
       
        $uniqueaccesslevel=db::table('accesslevel')->select('accesslevelname')->where([[DB::raw('upper(accesslevelname)'), $request->accesslevelname], ['accesslevelid', $request->accesslevelid]])->get();
        return response()->json(['data1'=> $data1, 'uniqueaccesslevel'=>$uniqueaccesslevel]);
    }

    public function showbuttons(Request $request)
    {

        $accessbuttons=db::select('select buttonid, buttonname, submoduleid from buttons');
        $allsubmodules=db::select('select submoduleid, submodulename from submodule');
        $checkedbuttons=db::select('select buttonid from accesslevelbutton where userid='. Auth::user()->id.' and accesslevelid='.$request->accesslevelid);
        return ['accessbuttons'=>$accessbuttons, 'allsubmodules'=>$allsubmodules, 'checkedbuttons'=>$checkedbuttons];
    }

    public function storeaccesslevelbutton(Request $request)
    {
        
        $accesslevelid= $request->accesslevelid; 
        $buttonid=[];
        $submoduleid=[];
        $accesslevelbutton=[];
        $buttonids=$request->buttonid;

        if(count($buttonids) != 0)
        {
             $buttonidandsubmoduleidinstring=implode(',', $buttonids);
            $buttonidandsubmoduleidinarray=explode(',', $buttonidandsubmoduleidinstring);
        
       
           foreach ($buttonidandsubmoduleidinarray as $buttonidandsubmoduleidkey=>$buttonidandsubmoduleid)
           {
            if($buttonidandsubmoduleidkey%2 == 0)
            {
                 array_push($buttonid,$buttonidandsubmoduleid);
            }
            else
            {
                 array_push($submoduleid,$buttonidandsubmoduleid);
            }

           }

            $uniquesubmoduleid=array_unique($submoduleid);
            $accesslevelbuttonidsindb=db::table('accesslevelbutton')->select('accesslevelbuttonid')->whereIn('submoduleid', $uniquesubmoduleid)->where([['accesslevelid', $accesslevelid]])->get();
           
            $accesslevelbuttonids=[];
            for($i=0; $i<count($accesslevelbuttonidsindb); $i++)
            {
                $accesslevelbuttonids[]=$accesslevelbuttonidsindb[$i]->accesslevelbuttonid;
            }

            if(count($accesslevelbuttonids) != 0)
            {
                
                db::table('accesslevelbutton')->whereIn('accesslevelbuttonid', $accesslevelbuttonids)->where([['accesslevelid', $accesslevelid]])->delete();
            }

          
          $accesslevelbuttonid=getMaxId('accesslevelbutton', 'accesslevelbuttonid');
           foreach ($buttonid as $buttonidkey => $buttonid) 
           {

                $accesslevelbutton[]=['accesslevelbuttonid'=>($accesslevelbuttonid), 'accesslevelid'=>$accesslevelid, 'submoduleid'=>$submoduleid[$buttonidkey], 'buttonid'=>$buttonid, 'userid'=>Auth::user()->id];
                $accesslevelbuttonid++;
     
             }
        }
        else
        {
            db::table('accesslevelbutton')->where([['accesslevelid', $accesslevelid]])->delete();
        }
          AccessLevelButton::insert($accesslevelbutton);
         return redirect()->back();
    }
}
