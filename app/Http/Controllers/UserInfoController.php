<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use DB;
use Image;
use App\AccessLevelUser;
use App\AccessLevel;
use App\ModuleUser;
use App\SubModuleUser;
use App\UserPermission;
use Hash;
use Auth;
use App\UserType;
use App\OfficeSetup;
use App\Department;
use Carbon\Carbon;



class UserInfoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=DB::table('users')->get();//select('*')->paginate(0);
        return view('userinfo.index')->withUsers($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usertype=DB::table('accesslevel')->pluck('accesslevelname', 'accesslevelid');
        $moduleusers = DB::Table("module")
                        ->select("module.*")->orderby('moduleid')
                        ->get(); 
        $submoduleusers = DB::Table("submodule")
                        ->select("submodule.*")->orderby('submoduleid')
                        ->get();             
        return view('userinfo.create')->withUsertype($usertype)->withModuleusers($moduleusers)->withSubmoduleusers($submoduleusers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'moduleid'=>'required',
            'submoduleid'=>'required',
        ));

        
        //return $request->all();
        $aaa=DB::table('users')
        ->where([
            ['email', '=', $request->email]
        ])  
        ->get();

        $existName=DB::table('users')
        ->where([
            ['name', '=', $request->name]
        ])  
        ->get();
        
        if(count($aaa)!=0)
        {
            $this->validate($request, array(
                'email'=>'required|max:100|unique:users,email'
            ));
        }
        else
        { 
            $this->validate($request, array(
               'email'=>'required|max:100'
               
            ));
        }

        if(count($existName)!=0)
        {
            $this->validate($request, array(
                'name'=>'required|max:100|unique:users,name'
            ));
        }
        else
        { 
            $this->validate($request, array(
               'name'=>'required|max:100'
               
            ));
        }

        $user=new User;

        $user->id=$request->id;
        $user->fullname=$request->fullname;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->contact=$request->contact;
        $user->phone=$request->phone;
        $user->usertypeid=$request->usertypeid;

        if($request->isactive != null)
        {
            $user->isactive=$request->isactive;
        }
        else
        {
             $user->isactive='N';
        }
       
        $image = $request->file( 'digitalsignature' );
        if($image)
        {
            $imageType = $image->getClientOriginalExtension();
            $imageStr =  (string) Image::make( $image )->
                                     resize( 200, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );


            $user->photo = base64_encode( $imageStr );
            $user->phototype = $imageType;
        } 
        $user->posted_by=Auth::user()->id;

        $user->modules()->sync($request->moduleid,false);
        $user->submodules()->sync($request->submoduleid,false);
        $user->save();

        Session::flash('success', 'User Information has been saved successfully.');
        return redirect()->route('userinfo.index');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::where([['id', $id]])->first();
         $moduleusers = DB::Table("module")
                        ->select("module.*",
                        DB::raw("(select 'checked' from moduleuser where module.moduleid = moduleuser.moduleid and moduleuser.id=$id)as status"))->orderby('moduleid')
                        ->get(); 
        $submoduleusers = DB::Table("submodule")
                        ->select("submodule.*",
                        DB::raw("(select 'checked' from submoduleuser where submodule.submoduleid = submoduleuser.submoduleid and submoduleuser.id=$id)as status"))->orderby('submoduleid')
                        ->get();                

        $accesslevelusers = DB::Table("accesslevel")
                        ->select("accesslevel.*",
                        DB::raw("(select 'checked' from accessleveluser where accesslevel.accesslevelid = accessleveluser.accesslevelid and accessleveluser.id=$id)as status"))->orderby('accesslevelid')
                        ->get();    
       
        return view('userinfo.show')->withUser($user)->withModuleusers($moduleusers)->withSubmoduleusers($submoduleusers)->withAccesslevelusers($accesslevelusers);
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::where([['id', $id]])->first();
        $usertypes=AccessLevel::get();
 

        $moduleusers = DB::Table("module")
                        ->select("module.*",
                        DB::raw("(select 'checked' from moduleuser where module.moduleid = moduleuser.moduleid and moduleuser.id=$id)as status"))->orderby('moduleid')
                        ->get(); 
        $submoduleusers = DB::Table("submodule")
                        ->select("submodule.*",
                        DB::raw("(select 'checked' from submoduleuser where submodule.submoduleid = submoduleuser.submoduleid and submoduleuser.id=$id)as status"))->orderby('submoduleid')
                        ->get();               

        $accesslevelusers = DB::Table("accesslevel")
                        ->select("accesslevel.*",
                        DB::raw("(select 'checked' from accessleveluser where accesslevel.accesslevelid = accessleveluser.accesslevelid and accessleveluser.id=$id)as status"))->orderby('accesslevelid')
                        ->get();    
       
        return view('userinfo.edit')->withUser($user)->withModuleusers($moduleusers)->withSubmoduleusers($submoduleusers)->withAccesslevelusers($accesslevelusers)->withUsertypes($usertypes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=User::where([['id', $id]])->first();

        $this->validate($request, array(
            'moduleid'=>'required',
            'submoduleid'=>'required',
        ));

        $aaa=DB::table('users')
        ->where([
            ['email', '=', $request->email]
        ])  
        ->get();

        $existName=DB::table('users')
        ->where([
            ['name', '=', $request->name]
        ])  
        ->get();
        
        if($request->email!= $user->email)
        {
            if(count($aaa)!=0)
            {
                $this->validate($request, array(
                    'email'=>'required|max:100|unique:users,email'
                   
                ));
            }
        }
        else
        {
            $this->validate($request, array(
               'email'=>'required|max:100'
               
            ));
        }

        if($request->name!= $user->name )
        {
            if(count($existName)!=0)
            {
                $this->validate($request, array(
                    'name'=>'required|max:100|unique:users,name'
                   
                ));
            }
        }
        else
        {
            $this->validate($request, array(
               'name'=>'required|max:100'
               
            ));
        }
        $user->name = $request->name;
        $user->fullname = $request->fullname;
       
        $user->email = $request->email;
        $user->contact = $request->contact;
        $user->phone = $request->phone;
        $user->usertypeid = $request->usertypeid;
        if($request->isactive != null)
        {
            $user->isactive = $request->isactive;
        }
        else
        {
            $user->isactive ='N';
        }
        
       
        $image = $request->file( 'digitalsignature' );
        if($image)
        {
            $imageType = $image->getClientOriginalExtension();
            $imageStr =  (string) Image::make( $image )->
                                     resize( 200, null, function ( $constraint ) {
                                         $constraint->aspectRatio();
                                     })->encode( $imageType );


            $user->photo= base64_encode( $imageStr );
            $user->phototype= $imageType;

        } 

    
         DB::table('moduleuser')->where('id', $id)->delete();
         
         DB::table('submoduleuser')->where('id',$id)->delete();

        

        $user->modules()->sync($request->moduleid,false);
        $user->submodules()->sync($request->submoduleid,false);

        $user->posted_by=Auth::user()->id;
        $user->save();

        Session::flash('success', 'User Information has been updated successfully');
         
        return redirect()->route('userinfo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uniqueemail(Request $request)
    {
        $data1=db::table('users')->select('email')->where([['email', $request->email]])->get();
       
        $uniqueemail=db::table('users')->select('email')->where([['email', $request->email], ['id', $request->id]])->get();
        return response()->json(['data1'=> $data1, 'uniqueemail'=>$uniqueemail]);

    }

    public function uniqueusername(Request $request)
    {
        $data1=db::table('users')->select('name')->where([[DB::raw('upper(name)'), $request->username]])->get();
       
        $uniqueusername=db::table('users')->select('name')->where([[DB::raw('upper(name)'), $request->username], ['id', $request->id]])->get();
        return response()->json(['data1'=> $data1, 'uniqueusername'=>$uniqueusername]);

    }

    public function changepasswordform()
    {

        return view('userinfo/changepassword');
    }

    public function changepassword(Request $request)
    {

          $this->validate($request, array(
                  'oldpassword' => 'required',
                'password' => 'required|confirmed'    
           
        ));

        $oldpassword = $request->oldpassword;
        $password = $request->password;
        
        if(!Hash::check($oldpassword, Auth::user()->password))
        {
             Session::flash('danger', 'Old Password is incorrect');
       
            return redirect('userinfo/user/changepassword');
        }

        $user = User::find(Auth::user()->id);
        // $user->password = bcrypt($password);
        $user->password=Hash::make($password);
        $user->save();
         Session::flash('success', 'Password has been changed successfully');
        return redirect('/');
    
    }

    public function usertypewisemodule(Request $request)
    {
        $umodules=DB::table('module')->select('moduleid', 'modulename')->where([['usertypeid', $request->usertypeid]])->get();
        $usubmodules=DB::select("select * from submodule sm, module m where sm.moduleid = m.moduleid and m.usertypeid =".$request->usertypeid);
        
        return ['umodules'=>$umodules, 'usubmodules'=>$usubmodules];
    }

    public function getModuleSubmoduleTreeView(Request $request)
    {
        $modulesubmodules = DB::Table("module")
                        ->select("module.*",
                        DB::raw("(select 'checked' from moduleuser where module.moduleid = moduleuser.moduleid and moduleuser.id=".$request->userid.")as status"))->orderby('moduleid')
                        ->get(); 
        foreach ($modulesubmodules as $key => $value) {
            $submodules = DB::Table("submodule")
                        ->select("submodule.*",
                        DB::raw("(select 'checked' from submoduleuser where submodule.submoduleid = submoduleuser.submoduleid and submoduleuser.id=".$request->userid.")as status"))->orderby('submoduleid')
                        ->get();  
            $value->submodules=$submodules;
        }
        return $modulesubmodules;
    }
  
}
