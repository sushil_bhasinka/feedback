<?php

namespace App\Http\Controllers;

use App\UserPermission;
use Illuminate\Http\Request;
use App\User;
use Session;
use DB;
use Auth;
use App\Module;
use App\SubModule;
use App\OfficeSetup;
use App\OfficeWiseDepartment;
use App\Department;
use App\ModuleUser;
use App\SubModuleUser;

class UserPermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display the specified resource.
     *
     * @param  \App\UserPermission  $userPermission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::FindOrFail($id);
        $moduleusers = DB::Table("module")
                        ->select("module.*",
                        DB::raw("(select 'checked' from moduleuser where module.moduleid = moduleuser.moduleid and moduleuser.id=$id)as status"))->orderby('moduleid')
                        ->get(); 
        $submoduleusers = DB::Table("submodule")
                        ->select("submodule.*",
                        DB::raw("(select 'checked' from submoduleuser where submodule.submoduleid = submoduleuser.submoduleid and submoduleuser.id=$id)as status"))->orderby('submoduleid')
                        ->get();             
        $offices = DB::Table("officesetups")
                        ->select("officesetups.*",
                        DB::raw("(select distinct 'checked' from userwiseofficedepartment where officesetups.officeid = userwiseofficedepartment.officeid and userwiseofficedepartment.userid=$id)as status"))->orderby('officeid')
                        ->get();
        $departments = DB::Table("departments")
                        ->select("departments.*",
                        DB::raw("(select 'checked' from userwiseofficedepartment where departments.departmentid = userwiseofficedepartment.departmentid and userwiseofficedepartment.userid=$id)as status"))->orderby('departmentid')
                        ->get();
                        
        return view('userinfo/userwiseofficedepartment')->withModuleusers($moduleusers)->withSubmoduleusers($submoduleusers)->withuser($user)->withoffices($offices)->withdepartments($departments);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'officeid'=>'required',
            'departmentid'=>'required',
            'moduleid'=>'required',
            'submoduleid'=>'required',
        ));
        //return $request->userid;
        DB::table('userwiseofficedepartment')->where('userid', $request->userid)->delete(); 

        foreach ($request->departmentid as $departid) {
            $userpermission = new UserPermission;
            $userpermission->userwiseofficedepartid=getMaxId('userwiseofficedepartment', 'userwiseofficedepartid');
            $userpermission->userid=$request->userid;
            $userpermission->officeid=$departid[0];
            $userpermission->departmentid=$departid[2];
            $userpermission->created_at=datetime();
            $userpermission->posted_by=Auth::user()->id;
            $userpermission->mac_id=GetMAC();
            $userpermission->save();
        }

        $user=User::where([['id', $request->userid]])->first();
        DB::table('moduleuser')->where('id', $request->userid)->delete();         
        DB::table('submoduleuser')->where('id',$request->userid)->delete();
        $user->modules()->sync($request->moduleid,false);
        $user->submodules()->sync($request->submoduleid,false);
        $user->save();

        Session::flash('success', 'User Permission added successfully');
        return redirect('/userinfo');
    }

    public function officewisedepartments(Request $request)
    {
        if($request->officeid){
        return $departments = DB::Table("officewisedepartment")->join('departments','departments.departmentid','=','officewisedepartment.departmentid')
                        ->select('officewisedepartment.departmentid','departments.departmentname','officewisedepartment.officeid',
                        DB::raw("(select distinct'checked' from userwiseofficedepartment where officewisedepartment.departmentid = userwiseofficedepartment.departmentid and userwiseofficedepartment.userid=".$request->userid.")as status"))->whereIn('officewisedepartment.officeid',$request->officeid)->orderby('departmentid')->groupBy('departments.departmentname','officewisedepartment.departmentid','officewisedepartment.departmentid','officewisedepartment.officeid')
                        ->get();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserPermission  $userPermission
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPermission $userPermission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserPermission  $userPermission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPermission $userPermission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserPermission  $userPermission
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPermission $userPermission)
    {
        //
    }
}
