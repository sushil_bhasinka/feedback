<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontEnd extends Model
{
    protected $table='feedbacks';
    protected $primaryKey='feedbackid';
    public $timestamps=false;

}
