<?php

use Illuminate\Session\SessionServiceProvider;
use Illuminate\Database\DatabaseServiceProvider;
use App\UserLog;
use App\UserPermission;
use carbon\carbon;


function orgid()
{ //set orgid for the controller
  //return 1;
  return 0;
}


function getMaxId($tableName, $columnName)
{
  $maxValues=DB::select('SELECT IFNULL(MAX('.$columnName.'),0)+1 AS maxid FROM '.$tableName );
  foreach($maxValues as $maxValue){
  return $maxValue->maxid;
  }
}

// increase primary key according to fiscalyear
function getPkId($tableName, $columnName,$FiscalYearId)
{
  $maxValues=DB::select('SELECT IFNULL(MAX('.$columnName.'),0)+1 AS id FROM '.$tableName.' where  fiscalyearid='.$FiscalYearId );
  foreach($maxValues as $maxValue){
  return $maxValue->id;
  }
}


function moneyFormat($num){
  $explrestunits = "" ;
  if(strlen($num)>6){
      $lastthree = substr($num, strlen($num)-6, strlen($num));
      $restunits = substr($num, 0, strlen($num)-6); // extracts the last three digits
      $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
      $expunit = str_split($restunits, 2);
      for($i=0; $i < sizeof($expunit);  $i++){
          // creates each of the 2's group and adds a comma to the end
          if($i==0)
          {
              $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
          }else{
              $explrestunits .= $expunit[$i].",";
          }
      }
      $thecash = $explrestunits.$lastthree;
  } else {
      $thecash = $num;
  }
  return $thecash; // writes the final format where $currency is the currency symbol.
}

function logdata($userid, $description, $module)
{
  $officeid = UserPermission::where('userid',$userid)->distinct('officeid')->pluck('officeid')->toArray();
  
	$userlog = new UserLog;
  $userlog->userlogid=getMaxId('userlogs','userlogid');
  $userlog->userid=$userid;
  $userlog->officeid=implode(',', $officeid);
  $userlog->description=$description;
  $userlog->module=$module;
  $userlog->logdatetime=carbon::now('Asia/Kathmandu');

  return $userlog->save();
}

function datetime(){
  return carbon::now('Asia/Kathmandu');
}

function GetMAC(){
    ob_start();
    system('getmac');
    $Content = ob_get_contents();
    ob_clean();
    return substr($Content, strpos($Content,'\\')-20, 17);
}
?>