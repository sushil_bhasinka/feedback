<?php

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    return redirect('/login');
});

Auth::routes();

Route::get('/' , 'FrontEndController@index');
Route::post('/sendfeedback' , 'FrontEndController@store');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/admin', 'HomeController@index')->name('home')->middleware('auth');
    
    //for access controll
    Route::resource('/accesslevels', 'AccessLevelController');
    Route::get('/uniqueaccesslevelname', 'AccessLevelController@uniqueaccesslevelname');
    Route::get('/showbuttons', 'AccessLevelController@showbuttons');
    Route::post('/accesslevels/storeaccesslevelbutton', 'AccessLevelController@storeaccesslevelbutton')->name('accesslevels.storeaccesslevelbutton');
    Route::resource('/modules', 'ModuleController');
    Route::get('/uniquemodulename', 'ModuleController@uniquemodulename');
    
    
    Route::resource('/submodules', 'SubModuleController');
    Route::get('/findsubmodules', 'SubModuleController@findsubmodules');//ajax function for detail table of submodule based on module
    Route::get('/uniquesubmodulename', 'SubModuleController@uniquesubmodulename');
    
    //for user information form
    Route::get ('/uniqueemail', 'UserInfoController@uniqueemail');
    Route::get ('/uniqueusername', 'UserInfoController@uniqueusername');
    Route::get('/userinfo/user/changepassword', 'UserInfoController@changepasswordform');
    Route::post('/userinfo/user/changepassword', 'UserInfoController@changepassword')->name('userinfo.changepassword');
    Route::get('/modulesubmoduletreeview', 'UserInfoController@getModuleSubmoduleTreeView');
    Route::resource('/userinfo', 'UserInfoController');
    Route::resource('/userpermission','UserPermissionController');
    Route::get('/officewisedepartments','UserPermissionController@officewisedepartments');
    
    
    Route::resource('/showpasswords', 'ShowPasswordController');
    Route::get('/usertypewisemodule', 'UserInfoController@usertypewisemodule');
    
    Route::get('/feedback','HomeController@feedback');
    Route::post('/feedback/confirmation','HomeController@confirmation');
    
    
});




// Route::get('/admin','DashboardController@index')->name('news.admin')->middleware('auth');

// Route::group(['prefix' => 'admin'], function () {
//     Auth::routes(['register'=>true]);
//     Route::resource('/mainmenu','MainMenuController');
//     // Route::resource('/post', 'PostController'); 
//     // Route::resource('/categories', 'CategoryController', ['except'=>['create']]);
//     // Route::resource('/tags','TagController',['except'=>['create']]);
//     // Route::resource('/mainmenu','MainMenuController', ['parameters'=>['mainmenu'=>'mainmenuid']]);
//     // Route::resource('invite', 'Account\InviteController',  ['parameters' => [
//         // 'invite' => 'account'
//     // ]]);
// });

// Route::group(['namespace'=>'Frontend'], function(){
//     Route::get('/', 'FrontendController@getHomePage');
//     // Route::get('/{slug}','NewsController@getSingleNews')->name('news.single');

//     // Route::post('comments/{postid}','CommentController@store')->name('comments.store');
// });