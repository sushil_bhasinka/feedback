@if(Session::has('noresult')==!null)
	<div class="alert alert-danger" role="alert">
		<strong>Sorry: </strong>{{Session::get('noresult')}}
	</div>
@endif