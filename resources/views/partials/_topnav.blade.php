<header class="main-header">
	<nav class="navbar navbar-static-top">
		{{-- <div class="container"> --}}
		{{-- <a href="../../index2.html" class="navbar-brand"> --}}

		{{-- <div class="navbar-header"> --}}

		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
			<i class="fa fa-bars"></i>
		</button>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="{{ url('/admin') }}">Home</a>
				</li>
				@foreach($modules as $module)
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$module->modulename}}</a>
					<ul class="dropdown-menu" role="menu">
						@foreach($submodules as $submodule)
						@if($submodule->moduleid==$module->moduleid)
						<li><a href="{{ url($submodule->url) }}">{{$submodule->submodulename}}</a></li>
						@endif
						@endforeach
					</ul>
				</li>
				@endforeach
			</ul>
		</div>
		<!-- /.navbar-collapse -->
		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account Menu -->
				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button -->
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!-- The user image in the navbar-->
						<img src="{{ asset('frontend/images/avatars/user-02.jpg') }}" class="user-image">
						<!-- hidden-xs hides the username on small devices so only the image appears. -->
						<span class="hidden-xs">{{Auth::user()->name}}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- Menu Body -->
						<li class="user-body">
							<div class="row">
								<div class="col-xs-12 text-center">
									<a href="{{url('/userinfo/user/changepassword')}}">Change Password</a>
								</div>
							</div>
						</li>
						<li class="user-body">
							<div class="row">
								<div class="col-xs-12 text-center">
									<a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
										{{ __('Logout') }}
									</a>
									<form id="logout-form" action="{{route('logout')}}" method="POST"
										style="display: none;">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</form>
								</div>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /.navbar-custom-menu -->
		{{-- </div> --}}
		<!-- /.container-fluid -->
	</nav>
</header>