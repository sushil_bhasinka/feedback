<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Admin Panel | Dashboard</title>

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- self developed styles (custom styling) -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css')}}">

<!-- Google Font -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/skin-blue.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">

<link rel="stylesheet" href="{{asset('plugins/nepali.datepicker.v3.2/css/nepali.datepicker.v3.2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/adminlte-datepicker/datepicker.min.css')}}">

<link rel="stylesheet" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables/responsive.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables/jquery.dataTables_themeroller.css')}}">
<link rel="stylesheet" href="{{asset('plugins/datatables/css/buttons.dataTables.min.css')}}">

<link rel="stylesheet" href="{{asset('css/treejs.css') }}">

@yield('stylesheets')