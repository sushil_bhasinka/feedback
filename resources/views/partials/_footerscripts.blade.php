<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/select2.min.js')}}"></script>
<script src="{{ asset('plugins/nepali.datepicker.v3.2/js/nepali.datepicker.v3.2.min.js') }}"></script>
<script src="{{ asset('plugins/adminlte-datepicker/datepicker.min.js') }}"></script>

<!-- for datatables  -->
<script src="{{asset('plugins/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables/responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables/js/buttons.print.min.js')}}"></script>

<script src="{{asset('js/treejs.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- for session time out  -->
<script type="text/javascript">
	$("document").ready(function(){
          $( ".alert" ).fadeOut( 5000 );
        });
</script>

<script>
	var url = window.location.href;
       // for sidebar menu entirely but not cover treeview
       $('ul.sidebar-menu a').filter(function() {
         return this.href == url;
       }).parent().addClass('active');
       // for treeview
       $('ul.treeview-menu a').filter(function() {
         return this.href == url;
       }).closest('.treeview').addClass('active');
</script>

