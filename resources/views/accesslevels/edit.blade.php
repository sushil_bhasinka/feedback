@extends('layouts.main')
@section('title', '|Edit Access Level')

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Access Levels</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" name="urlvalue" value="{{'/accesslevels/create'}}" class='activeurl'> 
      {!! Form::open(array( 'route'=>'accesslevels.store','data-parsley-validate' => '', 'files'=>'true')) !!}
      <div class="card-body">
        <div class="form-group">
          
          
          {{ Form::hidden('accesslevelid', getMaxId('accesslevel', 'accesslevelid')) }}
          {{-- <input type="hidden" name="academicyearid" value={{getMaxId('academicyear', 'academicyearid')}}> --}}
          {{Form::label('accesslevelname', 'Access Level:')}}
          {{Form::text('accesslevelname', null, array('class'=>'form-control accesslevelname' .($errors->has('accesslevelname') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Access Level'))}}
          @if($errors->has('accesslevelname'))
          <span class = "invalid-feedback">
            <strong>{{ 'Duplicate Access Level' }}</strong>
          </span>
          @endif
          <div id='accesslevelnameerror'></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <input type="submit" class="btn btn-primary" value="Save" id='saveaccesslevel'>
        </div>
      </div>
      {!! Form::close() !!}
				</div>

				<div class="col-md-6">

          <table id="tbl_accesslevels" class="table table-striped table-hover table-sm table-condensed">
            <thead class='bg-info'>
              <tr class="text-center">
                <th class="align-middle text-center" >S.N.</th>
                <th class="align-middle text-center" >Access Level</th>
                <th class="align-middle text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @php
              $count=1;
              @endphp
              @foreach($accesslevels as $accesslevel)
              <tr class='align-middle text-center'>
                {{-- <td >{{$accesslevel->accesslevelid}}</td> --}}
                <td >{{$count}}</td>
                <td >{{$accesslevel->accesslevelname}}</td>
                <td ><a href="/accesslevels/{{$accesslevel->accesslevelid}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                @php
                $count++;
                @endphp
                
              </tr>
              
              @endforeach
              
            </tbody>
          </table>
				</div>
			</div>
		</div>
</section>
@endsection

@section('content')
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Access Level</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      
    </div>
    <!-- /.card -->
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.accesslevelname',function(){
console.log("hmm its change");
var accesslevelname=$(this).val();
var accesslevelid=$('.accesslevelid').val();
//console.log(accesslevelname);
accesslevelname=accesslevelname.toUpperCase();
console.log(accesslevelname);
$.ajax({
type:'get',
url:'{!!URL::to('uniqueaccesslevelname')!!}',
data:{'accesslevelname':accesslevelname, 'accesslevelid':accesslevelid},
success:function(data){

console.log(data);
if(data.data1.length !=0)
{
if(data.uniqueaccesslevel.length==0)
{
document.getElementById('accesslevelnameerror').innerHTML='<p class="text-danger"><strong>Duplicate Access Level</strong> </p>';
$("#saveaccesslevel").prop("disabled",true);
}
else
{
document.getElementById('accesslevelnameerror').innerHTML='';
$("#saveaccesslevel").prop("disabled",false);
}

}
else
{
document.getElementById('accesslevelnameerror').innerHTML='';
$("#saveaccesslevel").prop("disabled",false);
}
},


error:function(){
//console.log('error');
}
});
});
});
</script>
@endsection