@extends('layouts.main')
@section('title', '|Access Level Entry')
@section('stylesheet')
  <style type="text/css">
    @media (min-width: 768px) {
    .modal-xl {
    width: 50%;
    max-width:1200px;
    }}
  </style>
@endsection

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Access Levels</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" name="urlvalue" value="{{'/accesslevels/create'}}" class='activeurl'> 
      {!! Form::open(array( 'route'=>'accesslevels.store','data-parsley-validate' => '', 'files'=>'true')) !!}
      <div class="card-body">
        <div class="form-group">
          
          
          {{ Form::hidden('accesslevelid', getMaxId('accesslevel', 'accesslevelid')) }}
          {{-- <input type="hidden" name="academicyearid" value={{getMaxId('academicyear', 'academicyearid')}}> --}}
          {{Form::label('accesslevelname', 'Access Level:')}}
          {{Form::text('accesslevelname', null, array('class'=>'form-control accesslevelname' .($errors->has('accesslevelname') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Access Level'))}}
          @if($errors->has('accesslevelname'))
          <span class = "invalid-feedback">
            <strong>{{ 'Duplicate Access Level' }}</strong>
          </span>
          @endif
          <div id='accesslevelnameerror'></div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <input type="submit" class="btn btn-primary" value="Save" id='saveaccesslevel'>
        </div>
      </div>
      {!! Form::close() !!}
				</div>

				<div class="col-md-6">

          <table id="tbl_accesslevels" class="table table-striped table-hover table-sm table-condensed">
            <thead class='bg-info'>
              <tr class="text-center">
                <th class="align-middle text-center" >S.N.</th>
                <th class="align-middle text-center" >Access Level</th>
                <th class="align-middle text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @php
              $count=1;
              @endphp
              @foreach($accesslevels as $accesslevel)
              <tr class='align-middle text-center'>
                {{-- <td >{{$accesslevel->accesslevelid}}</td> --}}
                <td >{{$count}}</td>
                <td >{{$accesslevel->accesslevelname}}</td>
                <td ><a href="/accesslevels/{{$accesslevel->accesslevelid}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                @php
                $count++;
                @endphp
                
              </tr>
              
              @endforeach
              
            </tbody>
          </table>
				</div>
			</div>
		</div>
</section>
@endsection

@section('script')

<script type="text/javascript">
	$(document).ready( function () {
		 $('#tbl_accesslevels').dataTable({
		ordering:false,
		 paginate:true
		 });
	} );
</script>

{{-- unique validation of accesslevel --}}
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.accesslevelname',function(){
console.log("hmm its change");
var accesslevelname=$(this).val();
//console.log(accesslevelname);
accesslevelname=accesslevelname.toUpperCase();
console.log(accesslevelname);
$.ajax({
type:'get',
url:'{!!URL::to('uniqueaccesslevelname')!!}',
data:{'accesslevelname':accesslevelname},
success:function(data){

console.log(data);
if(data.data1.length !=0)
{

document.getElementById('accesslevelnameerror').innerHTML='<p class="text-danger"><strong>Duplicate Access Level</strong> </p>';
$("#saveaccesslevel").prop("disabled",true);

}
else
{
document.getElementById('accesslevelnameerror').innerHTML='';
$("#saveaccesslevel").prop("disabled",false);
}
},


error:function(){
//console.log('error');
}
});
});
});
</script>

{{-- script for row select --}}
<script type="text/javascript">
   $(document).on("click", ".accesslevelrow", function(){

    $('.accesslevelrow').removeClass('selected');

    $(this).addClass('selected');
    var accesslevelid = $(this).find("td").eq(0).html();
    console.log(accesslevelid) ;
    // $(showstudents).attr("href",'/studentmasters/'+studentmasterid);
    // $(showallbuttonsform).attr("action",'accesslevels/'+accesslevelid);
    var ch='';
    var addbuttons='';
    $.ajax({
    type:'get',
    url:'{!!url::to('showbuttons')!!}',
    data:{'accesslevelid':accesslevelid},
     success:function(data)
     {
       $('.buttonincheckbox').html('');

       var submodulechecked='';

      console.log(data);
      for(var s=0; s<data.allsubmodules.length; s++)
      {
        submodulechecked='';
          for(var i=0; i<data.accessbuttons.length; i++)
        {
          
          if(data.allsubmodules[s].submoduleid == data.accessbuttons[i].submoduleid)
          { 
            checkedbtns=[]
            for(var c=0; c<data.checkedbuttons.length; c++)
            {
              checkedbtns.push(data.checkedbuttons[c].buttonid);
            }
            
            if(checkedbtns.length != 0)
            {
              if($.inArray(data.accessbuttons[i].buttonid , checkedbtns ) !== -1) 
              {
                
                addbuttons+='<li class="mb-2"><input type="checkbox" name="buttonid[]" value="'+data.accessbuttons[i].buttonid+','+data.allsubmodules[s].submoduleid +'" checked ><label>'+data.accessbuttons[i].buttonname+'</label></li>';
                submodulechecked='checked';
              }
              else
              {
                addbuttons+='<li class="mb-2"><input type="checkbox" name="buttonid[]" value="'+data.accessbuttons[i].buttonid+','+data.allsubmodules[s].submoduleid +'"><label>'+data.accessbuttons[i].buttonname+'</label></li>'
              }
            }
            else
            {
              submodulechecked='';
              addbuttons+='<li class="mb-2"><input type="checkbox" name="buttonid[]" value="'+data.accessbuttons[i].buttonid+','+data.allsubmodules[s].submoduleid +'"><label>'+data.accessbuttons[i].buttonname+'</label></li>'
            }
          }
          else
          {
            addbuttons='';
          }

        }
         
         ch+='<div class="col-md-4"><ul><li><input type="checkbox" name="submoduleid[]" value="'+data.allsubmodules[s].submoduleid+'" '+submodulechecked+'>'+data.allsubmodules[s].submodulename+ '<ul>' +addbuttons+ '</ul></li></ul></div>'
      }
      
      $('.buttonincheckbox').append(ch);
      $('.accesslevelid').append('<input type="hidden" name="accesslevelid" value="'+accesslevelid+'">');

        // for parent and child checkbox
        $(document).on("change", "li:has(li) > input[type='checkbox']", function() {
        $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
        });
        $(document).on("change", "input[type='checkbox'] ~ ul input[type='checkbox']", function() {
        $(this).closest("li:has(li)").children("input[type='checkbox']").prop('checked', $(this).closest('ul').find("input[type='checkbox']").is(':checked'));
        });
     },
     error:function()
     {

     }
    })
  });
</script>


@endsection