@extends('layouts.main')
@section('content')
<!-- Main content -->
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h3 class="box-title">Client Feedback</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <table id="tbl_modules" class="table table-striped table-hover table-sm table-condensed">
            <thead class='bg-info'>
              <tr class="text-center">
                <th class="align-middle">S.N.</th>
                <th class="align-middle">Client Name</th>
                <th class="align-middle">Gender</th>
                <th class="align-middle">Age</th>
                <th class="align-middle">Profession</th>
                <th class="align-middle">Date</th>
                <th class="align-middle">Action</th>
              </tr>
            </thead>
            <tbody>
              @php
              $count=1;
              @endphp
              @foreach($feedbacks as $feedback)
              <tr class='align-middle text-left'>
                <td>{{$count}}</td>
                <td>{{$feedback->clientname}}</td>
                <td>{{$feedback->question1}}</td>
                <td>{{$feedback->question2}}</td>
                <td >{{ $feedback->question3 }}</td>
                <td >{{ $feedback->date }}</td>
                <td ><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg{{$feedback->feedbackid}}">View</button>
                <div class="modal fade bd-example-modal-lg{{$feedback->feedbackid}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h2 class="modal-title">Client Feedback</h2>
                    </div>
                    <div class="modal-body">
                      <table class="table table-sm table-condensed table-bordered">
                        <tbody style="border: 1px solid;">
                          <tr>
                            <td class="align-middle"><label>Name: </label> {{ $feedback->clientname }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Gender: </label> {{ $feedback->question1 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Age: </label> {{ $feedback->question2 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Profession: </label> {{ $feedback->question3 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>How likely is it that you would recommend this company to a friend or colleague? </label><br> {{ $feedback->question4 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Overall, how satisfied or dissatisfied are you with our SERVICE? </label><br> {{ $feedback->question5 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Which of the following words would you use to describe our SERVICE? </label><br> {{ $feedback->question6 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>How well do our SERVICE meet your needs? </label><br> {{ $feedback->question7 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>How would you rate the quality of the SERVICE? </label><br> {{ $feedback->question8 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>How responsive have we been to your questions or concerns about our SERVICE? </label><br> {{ $feedback->question9 }}</td>
                          </tr>
                          <tr>
                            <td class="align-middle"><label>Do you have any other comments, questions, or concerns? </label><br>{{ $feedback->comments }}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div></td>
                @php
                $count++;
                @endphp
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</section>
<!-- /.box -->
@endsection

@section('script')
@endsection