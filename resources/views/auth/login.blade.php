<!DOCTYPE html>
<html lang="en">
<head>
	<title>Feedback System | Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>
<div id="login">
	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{asset('img/bg-01.jpg')}}');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-10 p-b-54">
				<form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
             		@csrf
					<div class="flex-col-c p-b-49">
						<span style="padding-top: 50px;"><h2 style="font-size: 40px!important; text-align: center;"><strong>Car Wash FeedBack System</strong></h2><br>
							<h2 style="padding-top: 20px;" class="text-center">Welcome</h2></span>
					</div>
					@if(session()->has('error'))
					    <div class="alert alert-danger">
					        {{ session()->get('error') }}
					    </div>
					@endif
					<div class="wrap-input100 validate-input mb-2" data-validate = "Username is reauired">
						<span class="label-input100">Username</span>
						<input class="input100 {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" name="username" value="{{ old('username') }}" placeholder="Type your Username" id="email">
						<p class="flex-col-c">
							@if ($errors->has('username'))
							 <span class="text-danger">
		                            {{ $errors->first('username') }}
		                        </span>
		                    @endif
		                </p>
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>
						

					<div class="wrap-input100 validate-input mb-2" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Type your password" id="password">
						<p>@if ($errors->has('password'))
	                        <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
	                    @endif</p>
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					
					<div class="container-login100-form-btn p-t-20">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit" value="Sign In">
								Login
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
	
	<script src="{{asset('/js/jquery.min.js')}}"></script>
	<!-- <script src="{{asset('/bootstrap/js/popper.js')}}"></script> -->
	<script src="{{asset('/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('/js/main.js')}}"></script>

</body>
</html>