<!DOCTYPE html>
<html lang="{{str_replace('_', '-', app()->getLocale()) }}">

<head>
     <title> {{config('app.name')}} @yield('title')</title>
     @include('partials._header')
</head>

<body class="hold-transition skin-blue fixed layout-top-nav">
     {{-- <body class="hold-transition skin-blue layout-top-nav"> --}}
     @include('partials._topnav')
     <div class="content-wrapper">
          @include('partials._messages')
          @yield('content')
     </div>
     {{-- @include('partials._mainfooter') --}}
     </div>
     @include('partials._footerscripts')

     @yield('script')
</body>

</html>