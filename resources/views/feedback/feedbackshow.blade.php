@extends('layouts.main')
@section('title', '|Feedback')
@section('stylesheet')
<style type="text/css">
</style>
@endsection

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Feedback Lists</h3>
		</div>
		<div class="box-body">
			<table id='tbl_feedback_lists' class="table table-hover table-striped table-condensed example" >
	            <thead class='bg-info'>
	              <tr class="text-center">
	                <th class="align-middle">S.N.</th>
	                <th class="align-middle">Client Name</th>
	                <th class="align-middle">Gender</th>
	                <th class="align-middle">Age</th>
	                <th class="align-middle">Profession</th>
	                <th class="align-middle">Date</th>
	                <th class="align-middle">Action</th>
	              </tr>
	            </thead>
	            <tbody>
	              @php
	              $count=1;
	              @endphp
	              @foreach($feedbacks as $feedback)
	              <tr class='align-middle text-left'>
	                <td>{{$count}}</td>
	                <td>{{$feedback->clientname}}</td>
	                <td>{{$feedback->question1}}</td>
	                <td>{{$feedback->question2}}</td>
	                <td >{{ $feedback->question3 }}</td>
	                <td >{{ $feedback->date }}</td>
	                <td ><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg{{$feedback->feedbackid}}">View</button>
	                <div class="modal fade bd-example-modal-lg{{$feedback->feedbackid}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	                <div class="modal-dialog modal-lg modal-dialog-centered">
	                  <div class="modal-content">
	                    <div class="modal-header">
	                      <form method="post" action="{{ url('/feedback/confirmation') }}">
	                      	@csrf
	                      <h2 class="modal-title">Client Feedback</h2>
	                      	<div class="text-right">
	                      		<input type="hidden" name="feedbackid" value="{{ $feedback->feedbackid }}">
	                      		<input type="submit" name="form_submit" value="Approved" class="btn btn-success btn-sm approved">
	                      		<input type="submit" name="form_submit" value="Ignored" class="btn btn-danger btn-sm ignored">
	                      	</div>
	                      </form>
	                    </div>
	                    <div class="modal-body">
	                      <table class="table table-sm table-condensed table-bordered">
	                        <tbody style="border: 1px solid;">
	                          <tr>
	                            <td class="align-middle"><label>Name: </label> {{ $feedback->clientname }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Gender: </label> {{ $feedback->question1 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Age: </label> {{ $feedback->question2 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Profession: </label> {{ $feedback->question3 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>How likely is it that you would recommend this company to a friend or colleague? </label><br> {{ $feedback->question4 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Overall, how satisfied or dissatisfied are you with our SERVICE? </label><br> {{ $feedback->question5 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Which of the following words would you use to describe our SERVICE? </label><br> {{ $feedback->question6 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>How well do our SERVICE meet your needs? </label><br> {{ $feedback->question7 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>How would you rate the quality of the SERVICE? </label><br> {{ $feedback->question8 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>How responsive have we been to your questions or concerns about our SERVICE? </label><br> {{ $feedback->question9 }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Do you have any other comments, questions, or concerns? </label><br>{{ $feedback->comments }}</td>
	                          </tr>
	                          <tr>
	                            <td class="align-middle"><label>Status</label><br>{{ $feedback->status }}</td>
	                          </tr>
	                        </tbody>
	                      </table>
	                    </div>
	                    <div class="modal-footer">
	                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                    </div>
	                  </div>
	                </div>
	              </div></td>
	                @php
	                $count++;
	                @endphp
	              </tr>
	              @endforeach
	            </tbody>
			</table>
		</div>
	</div>
</section>

@endsection


@section('script')

<script type="text/javascript">
$(document).ready( function () {
    $('#tbl_feedback_lists').dataTable({
	ordering:false,
    paginate:true
    });
} );
</script>
@endsection