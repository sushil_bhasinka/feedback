@extends('layouts.main')
@section('title', '|User Password Change')
@section('content')
<section class="content">
  <div class="box box-default">
    <div class="box-header">
      <h3 class="box-title">Change Password</h3>
    </div>
    <div class="box-body">
      <input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'>
      {!! Form::open(array('route'=>'userinfo.changepassword','data-parsley-validate' => '', 'files'=>'true')) !!} 
                  
            <div class="col-md-9">             
            <label for="current-password" class="col-sm-4 control-label text-right">Old Password</label>
            <div class="col-sm-8">
              <div class="form-group">
                <input class = 'form-control' type = 'password' name = 'oldpassword' required/>
                               </div>
            </div>
            <label for="password" class="col-sm-4 control-label text-right">New Password</label>
            <div class="col-sm-8">
              <div class="form-group">
                <input class = 'form-control' type = 'password' name = 'password' id='password'required />
              </div>
            </div>
            <label for="password_confirmation" class="col-sm-4 control-label text-right">Confirm Password</label>
            <div class="col-sm-8">
              <div class="form-group">
                <input class = 'form-control' type = 'password' name = 'password_confirmation' id='password_confirmation' required />
            <div id='message'></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-5 col-sm-6">
              <button type="submit" class="btn btn-primary">Change</button>
            </div>
          </div>
           </form>
    </div>
  </div>
</section>
@endsection
@section('script')
{{-- script for confirm password --}}
      <script type="text/javascript">

        $('#password, #password_confirmation').on('keyup', function () {
          //console.log($('#password').val()+'working');
          if(($('#password').val()).length!= 0)
          {
  if ( $('#password_confirmation').val()== $('#password').val()) {
    $('#message').html('Password Match').css('color', 'green');
  } else 
    $('#message').html('Password Does Not Match').css('color', 'red');
    }
    else
    {
      document.getElementById('message').innerHTML='';
    }
});
      </script>
@endsection


