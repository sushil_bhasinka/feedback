@extends('layouts.main')
@section('title', '|User Infromation')
@section('stylesheet')
<style type="text/css">
ul {
list-style-type: none;
}
</style>
@endsection
@section('content')
<div class='row'>
  <div class="col-md-12">
    <input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'>
    <a href='/userinfo/create'  role="button" class=' btn btn-info'><span class='text-white'>Add New User</span></a>
    <a href='/userinfo'  role="button" class=' btn btn-info pull-right'><span class='text-white'>All Users</span></a>
    <a href='/userinfo/user/changepassword'  role="button" class=' btn btn-info pull-right mr-2'><span class='text-white'>Change Password</span></a>
    
  </div>
</div>
<br>
<div class='row'>
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">User Information Entry</h3>
      </div>
      <table class="table ">
        <tr>
          <td style="width: 10%">UserName:</td>
          <td class="text-left">{{$user->name}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Contact:</td>
          <td class="text-left">{{$user->contact}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Email:</td>
          <td class="text-left">{{$user->email}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Phone:</td>
          <td class="text-left">{{$user->phone}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Expiry Date:</td>
          <td class="text-left">{{$user->expirydate}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Active:</td>
          <td class="text-left">{{$user->isactive}}</td>
        </tr>
      </table>
      <div class='row'>
        <div class="col-md-4">
          <div class='card card-info '>
            <div class='card-header d-flex p-1'>
              <h3 class="card-title">Modules/Submodules</h3>
              
            </div>
            
            <div class="checkbox">
              <ul>
                @foreach($moduleusers as $module)
                <li class="moduleid" >
                  @if($module->status === "checked")
                  <input type="checkbox" name="moduleid[]" value="{{ $module->moduleid }}" checked disabled="true">{{$module->modulename}}<br>
                  
                  @endif
                  <ul>
                    <div class="checkbox">
                      <li id="sub" class="sub" >
                        @foreach($submoduleusers as $submodule)
                        @if($submodule->status === "checked" AND $submodule->moduleid === $module->moduleid)
                        <input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid }}" checked disabled="true">{{$submodule->submodulename}}<br>
                        
                        @endif
                        @endforeach
                      </li>
                    </div>
                  </ul>
                </li>
                @endforeach
              </ul>
              
            </div>
            
          </div>
          
        </div>
        <div class='col-md-4'>
          
          <div class='card card-info '>
            <div class='card-header d-flex p-1'>
              <h3 class="card-title">Access Level</h3>
              
            </div>
            
            <div class="checkbox">
              <ul>
                @foreach($accesslevelusers as $access)
                @if($access->status === "checked")
                <input type="checkbox" name="accesslevelid[]" value="{{ $access->accesslevelid }}" checked disabled="true">{{$access->accesslevelname}}<br>
                
                @endif
                
                
                @endforeach
              </ul>
              
            </div>
            
          </div>
          
          
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript"></script>
@endsection