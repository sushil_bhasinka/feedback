@extends('layouts.main')
@section('title', '|User Information Edit')

@section('stylesheet')
<style type="text/css">
/*this css for input type=file*/
.filepointer {
   cursor: pointer;
   /* Style as you please, it will become the visible UI component. */
}

#photo {
   opacity: 0;
   position: absolute;
   z-index: -1;
}
ul {
list-style-type: none;
}
</style>
@endsection

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Edit User Information</h3>
		</div>
		<div class="box-body">
			<input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'>
      {!!Form::model($user, ['route'=>['userinfo.update', $user->id], 'method'=>'PUT', 'files'=>'true', 'data-parsley-validate' => ''])!!}
			<div class="card-body">
				<div class="form-group">
					<div class='row'>
            <div class="col-md-8">
              {{ Form::hidden('id', $user->id, ['class'=>'id']) }}
  						<div class='col-md-6'>
                {{Form::label('fullname', 'Full Name :')}}
                {{Form::text('fullname', null, array('class'=>'form-control' , 'required'=>'', 'placeholder'=>'Enter Full fullname'))}}
              </div>
              <div class='col-md-6'>
                {{Form::label('name', 'User Name :')}}
                {{Form::text('name', null, array('class'=>'form-control username' , 'required'=>'', 'placeholder'=>'Enter your Name'))}}
                @if($errors->has('name'))
                <span class = "text-danger">
                  <strong>{{ 'Duplicate Username' }}</strong>
                </span>
                @endif
                <div id='usernameerror'></div>
              </div>
              <div class='col-md-6'>
                {{Form::label('email', 'Email :')}}
                {{Form::email('email', null, array('class'=>'form-control email'.($errors->has('email') ? ' is-invalid' : '') , 'required'=>'', 'placeholder'=>'abc@___.com'))}}
                @if($errors->has('email'))
                <span class = "text-danger">
                  <strong>{{ 'Duplicate Email' }}</strong>
                </span>
                @endif
                <div id='emailerror'></div>
              </div>
              
              
              <div class='col-md-6'>
                {{Form::label('contact', 'Contact No.:')}} <span class="text-muted">(Number Only Upto 10)</span>
                {{Form::text('contact', null, array('class'=>'form-control , phonenumber' , 'required'=>'', 'placeholder'=>'Enter Contact','maxlength'=>'10'))}}
              </div>
              <div class='col-md-6'>
                {{Form::label('phone', 'Mobile No.:')}} <span class="text-muted">(Number Only Upto 10)</span>
                {{Form::text('phone', null, array('class'=>'form-control , phonenumber' , 'required'=>'', 'placeholder'=>'Enter Mobile No.','maxlength'=>'10'))}}
              </div>
              <div class='col-md-4'>
                {{Form::label('usertypeid', 'User Type:')}}
                <select class="form-control usertypeid" name='usertypeid'>
                  @foreach($usertypes as $usertype)
                  @if($user->usertypeid==$usertype->accesslevelid)
                  <option value='{{$usertype->accesslevelid}}' selected>{{$usertype->accesslevelname}}</option>
                  @else
                  <option value='{{$usertype->accesslevelid}}' >{{$usertype->accesslevelname}}</option>
                  @endif
                  @endforeach
                  
                </select>
              </div>
              <div class='col-md-2 mt-4'>
                @if($user->isactive == 'Y' )
                <input type="checkbox" name="isactive" value="Y" checked="true">
                @else
                <input type="checkbox" name="isactive" value="Y" >
                @endif
                
                {{Form::label('isactive', 'Is Active')}}
              </div> 
            </div>
            <div class="col-md-4">
              <div class='card card-info '>
                <div class="box box-default">
                  <div class="box-header">
                    <h3 class="box-title">Modules/Submodules</h3>
                  </div>
                  <div class="box-body">
                    <div class="modulesubmodule" id="modulesubmodule"></div>
                 <div class="checkbox">
                        @if($errors->has('moduleid'))
                        <span class = "text-danger">
                          <strong class="ml-5">{{ 'Module Required' }}</strong>
                        </span>
                        @endif<br>
                        @if($errors->has('submoduleid'))
                        <span class = "text-danger">
                          <strong class="ml-5">{{ 'SubModule Required' }}</strong>
                        </span>
                        @endif
                      <ul>
                          @foreach($moduleusers as $module)
                          <li class="moduleid" >
                            @if($module->status === "checked")
                            <input type="checkbox" name="moduleid[]" value="{{ $module->moduleid }}"  checked class="{{ $errors->has('moduleid') ? ' is-invalid' : '' }}" > <strong>{{$module->modulename}}</strong><br>
                            @else
                            <input type="checkbox" name="moduleid[]" value="{{ $module->moduleid  }}" {{ (is_array(old('moduleid')) and in_array( $module->moduleid, old('moduleid'))) ? ' checked' : '' }} class="{{ $errors->has('moduleid') ? ' is-invalid' : '' }}" > <strong>{{$module->modulename}}</strong>
                            @endif
                            <ul>
                                  @foreach($submoduleusers as $submodule)
                                  @if($submodule->status === "checked" && $submodule->moduleid === $module->moduleid)
                                  <li id="sub" class="sub" >
                                  <input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid }}" class="{{ $errors->has('submoduleid') ? ' is-invalid' : '' }}" checked> 
                                  {{$submodule->submodulename}}</li>
                                  @elseif($submodule->moduleid === $module->moduleid)
                                  <li id="sub" class="sub" >
                                  <input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid  }}" {{ (is_array(old('submoduleid')) and in_array( $submodule->submoduleid, old('submoduleid'))) ? ' checked' : '' }} class="{{ $errors->has('submoduleid') ? ' is-invalid' : '' }}"> {{$submodule->submodulename}}
                                  </li>
                                  @endif
                                  @endforeach
                            </ul>
                          </li>
                          @endforeach
                        </ul>
                   </div>
                  </div>
                </div>
              </div>
            </div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
      <input type="submit" class="btn btn-primary" value='Save' id='saveuser'>
      <a href="{{'/userinfo'}}"><button type="button" class="btn btn-outline-primary float-right">Back</button></a>
    </div>
	  {!! Form::close() !!}
	</div>
</section>
@endsection

@section('script')
{{-- show the selected image in form --}}
<script type="text/javascript">
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function (e) {
$('#blah')
.attr('src', e.target.result)
.width(100)
.height(125);
};
reader.readAsDataURL(input.files[0]);
}
}

</script>
{{-- script for confirm password --}}
<script type="text/javascript">
$('#password, #passwordconfirm').on('keyup', function () {
if ( $('#passwordconfirm').val()== $('#password').val()) {
$('#message').html('Password Matching').css('color', 'green');
} else
$('#message').html('Password does Not Matching').css('color', 'red');
});
</script>
<script type="text/javascript">

$(function() {

$(document).on("change", "li:has(li) > input[type='checkbox']", function() {
$(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
});
$(document).on("change", "input[type='checkbox'] ~ ul input[type='checkbox']", function() {
$(this).closest("li:has(li)").children("input[type='checkbox']").prop('checked', $(this).closest('ul').find("input[type='checkbox']").is(':checked'));
});
})
</script>
{{-- unique validation of email --}}
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.email',function(){
console.log("hmm its change");
var email=$(this).val();
var id=$('.id').val();
//console.log(email);

console.log(email);
$.ajax({
type:'get',
url:'{!!URL::to('uniqueemail')!!}',
data:{'email':email, 'id':id},
success:function(data){
//console.log('success');
console.log(data);
//console.log(data.length);
if(data.data1.length !=0)
{
if(data.uniqueemail.length==0)
{
document.getElementById('emailerror').innerHTML='<p class="text-danger"><strong>Duplicate Email </strong> </p>';
$("#saveuser").prop("disabled",true);
}
else
{
document.getElementById('emailerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}


// }
}
else
{
document.getElementById('emailerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});
</script>

{{-- unique validation of username --}}
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.username',function(){

var username=$(this).val().toUpperCase();
console.log(username);

var id=$('.id').val();
$.ajax({
type:'get',
url:'{!!URL::to('uniqueusername')!!}',
data:{'username':username, 'id':id},
success:function(data){
if(data.data1.length !=0)
{
if(data.uniqueusername.length==0)
{
document.getElementById('usernameerror').innerHTML='<p class="text-danger"><strong>Duplicate Username </strong> </p>';
$("#saveuser").prop("disabled",true);
}
else
{
document.getElementById('usernameerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}

}
else
{
document.getElementById('usernameerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});
</script>
{{-- for usertype wise module access --}}
<script type="text/javascript">
  
$(document).ready(function(){
  var firstusertypeid=$('.usertypeid').val();
$(document).on('click','.usertypeid',function(){
  $('#checkbox').css('display', 'none');
console.log("hmm its change");
var usertypeid=$(this).val();
console.log('firstusertypeid'+firstusertypeid);
console.log('currentusertypeid'+usertypeid);
//console.log(usertypeid);

console.log(usertypeid);
if(firstusertypeid == usertypeid)
{
  $('#ajaxcheckbox').css('display', 'none');
  $('#checkbox').css('display', 'block');
}
else
{
  console.log('ajaxcheckbox is working');
  $.ajax({
  type:'get',
  url:'{!!URL::to('usertypewisemodule')!!}',
  data:{'usertypeid':usertypeid},
  success:function(data){
  //console.log('success');
  console.log(data);
  $('#ajaxcheckbox').css('display', 'block');
  var html='';

  if(data.umodules.length !=0)
  {
     html+='<ul>';
    for(var i=0; i<data.umodules.length; i++)
    {
      html+='<li class="module" id="module" ><input type="checkbox" value="'+data.umodules[i].moduleid+'" class="moduleid icheckbox_flat-green" id="moduleid" name="moduleid[]" ><label>'+data.umodules[i].modulename+'</label><ul id="sub">';
     

      for(var j=0; j<data.usubmodules.length; j++)
      {
        if(data.umodules[i].moduleid === data.usubmodules[j].moduleid)
        {
          html+='<li class="submodule" id="sub"><input type="checkbox" value="'+ data.usubmodules[j].submoduleid+ '" id="submoduleid " name="submoduleid[]" class="icheckbox_flat-green">'+data.usubmodules[j].submodulename+'</li>';
        }
      }
      html+='</ul></li>';
    }
    html+='</ul>';
  }
  else
  {
    html='<div class="alert alert-danger">NO Module For User</div>';
  }
  $('#ajaxcheckbox').html(html);

  //console.log(data.length);
  // if(data.umodules.length !=0)
  // {

  // document.getElementById('usertypeiderror').innerHTML='<p class="text-danger"><strong>Duplicate usertypeid </strong> </p>';
  // $("#saveuser").prop("disabled",true);
  // // }
  // }
  // else
  // {
  // document.getElementById('emailerror').innerHTML='';
  // $("#saveuser").prop("disabled",false);
  // }
  },
  error:function(){
  //console.log('error');
  }
  });
}

});
});

</script>

<script type="text/javascript">
$(document).ready(function() {
var userid=$('.id').val();

$.ajax({
  type:'get',
  url:'{!!URL::to('officedepartmenttreeview')!!}',
  data:{'userid':userid},
  success:function(data){
    //console.log(data)
    let jsonData = data;
    $("#officedepartment").treejs({
        sourceType  : 'json',
        dataSource  : jsonData,
        initialState: 'close'
    });

    // To check JSON format
    $('#check_json_format').click(function(){
        $('#json_format_container').toggle();
    });
  },
  error:function(){
  //console.log('error');
  }
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  var userid=$('.id').val();
  var officeid =[];
   if($('input[name="officeid[]"]').prop("checked") == true){
     $('input[name="alloffice"]').prop("checked",'checked')
    }

  if($('input[name="officeid[]"]').prop("checked") == true){
      officeid.push($('input[name="officeid[]"]').val())
    }
    else if($('input[name="officeid[]"]').prop("checked") == false){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $('input[name="officeid[]"]').val()) { 
            officeid.splice(i, 1); 
          }
        }
    }
    officewisedepartment(officeid)


  $('input[name="officeid[]"]').click(function(){
  
    if($(this).prop("checked") == true){
      officeid.push($(this).val())
        
    }
    else if($(this).prop("checked") == false){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $(this).val()) { 
            officeid.splice(i, 1); 
          }
        }
    }
      officewisedepartment(officeid)
  });

  $('input[name="alloffice"]').click(function(){
  
    if($('input[name="alloffice"]').prop("checked") === true){
      $('input[name="officeid[]"]').each(function(){
        officeid.push($(this).val())
      })
    }
    else if($('input[name="alloffice"]').prop("checked") === false){
      $('input[name="officeid[]"]').each(function(){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $(this).val()) { 
            officeid.splice(i, 1); 
          }
        }
      })
    }
    //console.log(officeid)
      officewisedepartment(officeid)
  });
});

function officewisedepartment(officeid){
  var userid=$('.id').val();
  $.ajax({
      type:'get',
      url:'{!!URL::to('officewisedepartments')!!}',
      data:{'officeid':officeid,'userid':userid},
      success:function(data){
        console.log(data)
        var departmentdata='<ul><li><input type="checkbox" name="alldepartment"><label>Checked All</label><ul style="list-style-type: none;">';
        for (var i = 0; i < data.length; i++) {
          if(data[i].status == 'checked'){
            departmentdata += '<li class="departmentid" id="department"><input type="checkbox" value="'+data[i].departmentid+'" class="departmentid {{ $errors->has("departmentid") ? " is-invalid" : " " }}" id="departmentid" name="departmentid[]" checked=""> <label>'+data[i].departmentname+'</label></li>'
          }
          else
          {
            departmentdata += '<li class="departmentid" id="department"><input type="checkbox" value="'+data[i].departmentid+'" {{ (is_array(old("departmentid")) and in_array( '+data[i].departmentid+', old("departmentid"))) ? " checked" : " " }} class="departmentid {{ $errors->has("departmentid") ? " is-invalid" : " " }}" id="departmentid" name="departmentid[]"> <label>'+data[i].departmentname+'</label></li>'
          }
        }
          departmentdata += '</ul></li></ul>';
        $('.departmentdata').html(departmentdata)
        
        if($('input[name="departmentid[]"]').prop("checked") == true){
          $('input[name="alldepartment"]').prop("checked",'checked')
        }
        //console.log(departmentdata)
      },
      error:function(){
      //console.log('error');
      }
      });
}
</script>
@endsection