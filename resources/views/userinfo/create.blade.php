@extends('layouts.main')
@section('title', '|User Information Entry')

@section('stylesheet')
<style type="text/css">
/*this css for input type=file*/
.filepointer {
   cursor: pointer;
   /* Style as you please, it will become the visible UI component. */
}

#photo {
   opacity: 0;
   position: absolute;
   z-index: -1;
}
</style>
@endsection

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Add New User</h3>
		</div>
		<div class="box-body">
			<input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'>
			{!! Form::open(array( 'route'=>'userinfo.store','data-parsley-validate' => '', 'files'=>'true')) !!}
			<div class="card-body">
				<div class="form-group">
					<div class='row'>
						<div class="col-md-8">
							{{ Form::hidden('id', getMaxId('users', 'id')) }}
							<div class='col-md-6'>
								{{Form::label('fullname', 'Full Name :')}}
								{{Form::text('fullname', null, array('class'=>'form-control' , 'required'=>'', 'placeholder'=>'Enter Full fullname'))}}
							</div>
							<div class='col-md-6'>
								{{Form::label('name', 'User Name :')}}
								{{Form::text('name', null, array('class'=>'form-control username' , 'required'=>'', 'placeholder'=>'Enter your Name'))}}
								@if($errors->has('name'))
								<span class="text-danger">
									<strong>{{ 'Duplicate Username' }}</strong>
								</span>
								@endif
								<div id='usernameerror'></div>
							</div>
							<div class='col-md-6'>
								{{Form::label('password', 'Password :')}}
								{{Form::password('password',array('class'=>'form-control password' , 'required'=>'', 'placeholder'=>'Enter your password'))}}
							</div>
							<div class='col-md-6'>
								{{Form::label('passwordconfirm', 'Confirm Password :')}}
								{{ Form::password('passwordconfirm', ['class' => 'form-control passwordconfirm','placeholder'=>'Re-enter your Password']) }}
								<div id='message'></div>
							</div>
							<div class='col-md-6'>
								{{Form::label('email', 'Email :')}}
								{{Form::email('email', null, array('class'=>'form-control email'.($errors->has('email') ? ' is-invalid' : '') , 'required'=>'', 'placeholder'=>'abc@___.com'))}}
								@if($errors->has('email'))
								<span class="text-danger">
									<strong>{{ 'Duplicate Email' }}</strong>
								</span>
								@endif
								<div id='emailerror'></div>
							</div>
							<div class='col-md-6'>
								{{Form::label('contact', 'Contact No.:')}} <span class="text-muted">(Number Only Upto 10)</span>
								{{Form::text('contact', null, array('class'=>'form-control , phonenumber' , 'required'=>'', 'placeholder'=>'021-1234567','maxlength'=>'11'))}}
							</div>
							<div class='col-md-6'>
								{{Form::label('phone', 'Mobile No.:')}} <span class="text-muted">(Number Only Upto 10)</span>
								{{Form::text('phone', null, array('class'=>'form-control , phonenumber' , 'required'=>'', 'placeholder'=>'Enter Mobile No.','maxlength'=>'10'))}}
							</div>
							<div class='col-md-4'>
								{{Form::label('usertypeid', 'User Type:')}}
								{!! Form::select('usertypeid', $usertype, null, ['class' => 'form-control usertypeid',
								'placeholder' =>
								'Select User Type', 'id'=>'usertypeid','required'=>'']) !!}
							</div>
							<div class='col-md-2 mt-4'>
								{{Form::checkbox('isactive','Y', ['checked'=>'true'])}}
								{{Form::label('isactive', 'Is Active')}}
							</div>
						</div>
						<div class="col-md-4 ">
							<div class='card card-info '>
								<div class="box box-default">
									<div class="box-header">
										<h3 class="box-title">Modules/Submodules</h3>
									</div>
									<div class="box-body">
										<div class="modulesubmodule" id="modulesubmodule"></div>
										<div class="checkbox">
											@if($errors->has('moduleid'))
											<span class="text-danger">
												<strong class="ml-5">{{ 'Module Required' }}</strong>
											</span>
											@endif<br>
											@if($errors->has('submoduleid'))
											<span class="text-danger">
												<strong class="ml-5">{{ 'SubModule Required' }}</strong>
											</span>
											@endif
											<ul>
												@foreach($moduleusers as $module)
												<li class="moduleid">
													<input type="checkbox" name="moduleid[]" value="{{ $module->moduleid  }}"
														{{ (is_array(old('moduleid')) and in_array( $module->moduleid, old('moduleid'))) ? ' checked' : '' }}
														class="{{ $errors->has('moduleid') ? ' is-invalid' : '' }}">
													<strong>{{$module->modulename}}</strong>
													<ul>
														@foreach($submoduleusers as $submodule)
														@if($submodule->moduleid === $module->moduleid)
														<li id="sub" class="sub">
															<input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid  }}"
																{{ (is_array(old('submoduleid')) and in_array( $submodule->submoduleid, old('submoduleid'))) ? ' checked' : '' }}
																class="{{ $errors->has('submoduleid') ? ' is-invalid' : '' }}">
															{{$submodule->submodulename}}
														</li>
														@endif
														@endforeach
													</ul>
												</li>
												@endforeach
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary"  id='saveuser'>Save</button>
			<a href="{{'/userinfo'}}"><button type="button" class="btn btn-outline-primary float-right">Back</button></a>
		 </div>
	  {!! Form::close() !!}
	</div>

	
</section>
@endsection


@section('script')
 <!-- show the selected image in form -->
<script type="text/javascript">
function readURL(input) {
if (input.files && input.files[0]) {
var reader = new FileReader();
reader.onload = function (e) {
$('#blah')
.attr('src', e.target.result)
.width(100)
.height(125);
};
reader.readAsDataURL(input.files[0]);
}
}
</script>

<!--  script for confirm password -->
<script type="text/javascript">
$('#password, #passwordconfirm').on('keyup', function () {
if ( $('#passwordconfirm').val()== $('#password').val()) {
$('#message').html('Password Matching').css('color', 'green');
} else
$('#message').html('Password does Not Matching').css('color', 'red');
});
</script>

 <!-- script for parent and child checkbox -->
<script type="text/javascript">

$(function() {

$(document).on("change", "li:has(li) > input[type='checkbox']", function() {
$(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
});
$(document).on("change", "input[type='checkbox'] ~ ul input[type='checkbox']", function() {
$(this).closest("li:has(li)").children("input[type='checkbox']").prop('checked', $(this).closest('ul').find("input[type='checkbox']").is(':checked'));
});
})
</script>
<!--  unique validation of email -->
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.email',function(){
var email=$(this).val();
//console.log(email);

$.ajax({
type:'get',
url:'{!!URL::to('uniqueemail')!!}',
data:{'email':email},
success:function(data){
if(data.data1.length !=0)
{

document.getElementById('emailerror').innerHTML='<p class="text-danger"><strong>Duplicate Email </strong> </p>';
$("#saveuser").prop("disabled",true);
// }
}
else
{
document.getElementById('emailerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});
</script>

<!--  unique validation of username -->
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.username',function(){
var username=$(this).val().toUpperCase();
console.log(username);

$.ajax({
type:'get',
url:'{!!URL::to('uniqueusername')!!}',
data:{'username':username},
success:function(data){
  console.log(data.data1)
if(data.data1.length !=0)
{

document.getElementById('usernameerror').innerHTML='<p class="text-danger"><strong>Duplicate Username </strong> </p>';
$("#saveuser").prop("disabled",true);
// }
}
else
{
document.getElementById('usernameerror').innerHTML='';
$("#saveuser").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});
</script>
 <!-- usertype wise module and submodule -->
<script type="text/javascript">
  
$(document).ready(function(){
$(document).on('change','.usertypeid',function(){

var usertypeid=$(this).val();
//console.log(usertypeid);

//console.log(usertypeid);
$.ajax({
type:'get',
url:'{!!URL::to('usertypewisemodule')!!}',
data:{'usertypeid':usertypeid},
success:function(data){
//console.log('success');
//console.log(data);
var html='';
if(data.umodules.length !=0)
{
   html+='<ul style="list-style-type: none;">';
  for(var i=0; i<data.umodules.length; i++)
  {
    html+='<li class="module" id="module"><input type="checkbox" value="'+data.umodules[i].moduleid+'" class="moduleid icheckbox_flat-green" id="moduleid" name="moduleid[]" ><label>'+data.umodules[i].modulename+'</label><ul id="sub">';
   

    for(var j=0; j<data.usubmodules.length; j++)
    {
      if(data.umodules[i].moduleid === data.usubmodules[j].moduleid)
      {
        html+='<li style="list-style-type: none;" class="submodule" id="sub"><input type="checkbox" value="'+ data.usubmodules[j].submoduleid+ '" id="submoduleid " name="submoduleid[]" class="icheckbox_flat-green">'+data.usubmodules[j].submodulename+'</li>';
      }
    }
    html+='</ul></li>';
  }
  html+='</ul>';
}
else
{
  html='<div class="alert alert-danger">NO Module For User</div>';
}

$('#checkbox').html(html);
},
error:function(){
//console.log('error');
}
});
});
});

</script>


@endsection