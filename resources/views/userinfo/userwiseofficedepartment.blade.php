@extends('layouts.main')
@section('title', '|User Permission')
@section('stylesheet')
<style type="text/css">
ul {
list-style-type: none;
}
</style>
@endsection
@section('content')
<div class='row'>
  <div class="col-md-12">
    <input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'>
    <a href='/userinfo/create'  role="button" class=' btn btn-info'><span class='text-white'>Add New User</span></a>
    <a href='/userinfo'  role="button" class=' btn btn-info pull-right'><span class='text-white'>All Users</span></a>
    <a href='/userinfo/user/changepassword'  role="button" class=' btn btn-info pull-right mr-2'><span class='text-white'>Change Password</span></a>
    
  </div>
</div>
<br>
<div class='row'>
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">User Information Entry</h3>
      </div>
      <table class="table ">
        <tr>
          <td style="width: 10%">UserName:</td>
          <td class="text-left">{{$user->name}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Contact:</td>
          <td class="text-left">{{$user->contact}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Email:</td>
          <td class="text-left">{{$user->email}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Phone:</td>
          <td class="text-left">{{$user->phone}}</td>
        </tr>
        <tr>
          <td style="width: 10%">Active:</td>
          <td class="text-left">{{$user->isactive}}</td>
        </tr>
      </table>
      {!! Form::open(array( 'route'=>'userpermission.store','data-parsley-validate' => '', 'files'=>'true')) !!} 
      <input type="hidden" name="userid" value="{{$user->id}}" class="userid">
      <div class='row'>
        <div class='col-md-6 '>
          <div class='card card-info '>
            <div class='card-header  '>
              <h3 class="card-title">Offices/Departments</h3>
            </div>
            <div class="checkbox">
              @if($errors->has('departmentid'))
                  <span class = "text-danger">
                    <strong class="ml-5">{{ 'Department Required' }}</strong>
                  </span>
                  @endif
                <!-- <div class="departmentdata">
               
                </div> -->
                <div class="officedepartment" id="officedepartment">
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 ">
          <div class='card card-info '>
            <div class='card-header  '>
              <h3 class="card-title">Modules/Submodules</h3>
            </div>
            <div class="modulesubmodule" id="modulesubmodule"></div>
           <div class="checkbox">
                  @if($errors->has('moduleid'))
                  <span class = "text-danger">
                    <strong class="ml-5">{{ 'Module Required' }}</strong>
                  </span>
                  @endif<br>
                  @if($errors->has('submoduleid'))
                  <span class = "text-danger">
                    <strong class="ml-5">{{ 'SubModule Required' }}</strong>
                  </span>
                  @endif
                <ul>
                    @foreach($moduleusers as $module)
                    <li class="moduleid" >
                      @if($module->status === "checked")
                      <input type="checkbox" name="moduleid[]" value="{{ $module->moduleid }}"  checked class="{{ $errors->has('moduleid') ? ' is-invalid' : '' }}" > <strong>{{$module->modulename}}</strong><br>
                      @else
                      <input type="checkbox" name="moduleid[]" value="{{ $module->moduleid  }}" {{ (is_array(old('moduleid')) and in_array( $module->moduleid, old('moduleid'))) ? ' checked' : '' }} class="{{ $errors->has('moduleid') ? ' is-invalid' : '' }}" > <strong>{{$module->modulename}}</strong>
                      @endif
                      <ul>
                            @foreach($submoduleusers as $submodule)
                            @if($submodule->status === "checked" && $submodule->moduleid === $module->moduleid)
                            <li id="sub" class="sub" >
                            <input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid }}" class="{{ $errors->has('submoduleid') ? ' is-invalid' : '' }}" checked> 
                            {{$submodule->submodulename}}</li>
                            @elseif($submodule->moduleid === $module->moduleid)
                            <li id="sub" class="sub" >
                            <input type="checkbox" name="submoduleid[]" value="{{ $submodule->submoduleid  }}" {{ (is_array(old('submoduleid')) and in_array( $submodule->submoduleid, old('submoduleid'))) ? ' checked' : '' }} class="{{ $errors->has('submoduleid') ? ' is-invalid' : '' }}"> {{$submodule->submodulename}}
                            </li>
                            @endif
                            @endforeach
                      </ul>
                    </li>
                    @endforeach
                  </ul>
             </div>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary" >Save</button>
        
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(function() {
  $(document).on("change", "li:has(li) > input[type='checkbox']", function() {
    $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
  });
  $(document).on("change", "input[type='checkbox'] ~ ul input[type='checkbox']", function() {
    $(this).closest("li:has(li)").children("input[type='checkbox']").prop('checked', $(this).closest('ul').find("input[type='checkbox']").is(':checked'));
  });
})
</script>

<script type="text/javascript">
$(document).ready(function() {
var userid=$('.userid').val();

$.ajax({
  type:'get',
  url:'{!!URL::to('officedepartmenttreeview')!!}',
  data:{'userid':userid},
  success:function(data){
    //console.log(data)
    let jsonData = data;
    $("#officedepartment").treejs({
        sourceType  : 'json',
        dataSource  : jsonData,
        initialState: 'close'
    });

    // To check JSON format
    $('#check_json_format').click(function(){
        $('#json_format_container').toggle();
    });
  },
  error:function(){
  //console.log('error');
  }
  });
});
</script>

<!-- <script type="text/javascript">
$(document).ready(function() {
var userid=$('.userid').val();

$.ajax({
  type:'get',
  url:'{!!URL::to('modulesubmoduletreeview')!!}',
  data:{'userid':userid},
  success:function(data){
    console.log(data)
    let jsonData = data;
    $("#modulesubmodule").treejs({
        sourceType  : 'json',
        dataSource  : jsonData,
        initialState: 'close',
        moduleType:'modulesubmodule'
    });

    // To check JSON format
    $('#check_json_format').click(function(){
        $('#json_format_container').toggle();
    });
  },
  error:function(){
  //console.log('error');
  }
  });
});
</script> -->

<script type="text/javascript">
$(document).ready(function(){
  var userid=$('.userid').val();
  var officeid =[];
   if($('input[name="officeid[]"]').prop("checked") == true){
     $('input[name="alloffice"]').prop("checked",'checked')
    }

  if($('input[name="officeid[]"]').prop("checked") == true){
      officeid.push($('input[name="officeid[]"]').val())
    }
    else if($('input[name="officeid[]"]').prop("checked") == false){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $('input[name="officeid[]"]').val()) { 
            officeid.splice(i, 1); 
          }
        }
    }
    officewisedepartment(officeid)


  $('input[name="officeid[]"]').click(function(){
  
    if($(this).prop("checked") == true){
      officeid.push($(this).val())
        
    }
    else if($(this).prop("checked") == false){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $(this).val()) { 
            officeid.splice(i, 1); 
          }
        }
    }
      officewisedepartment(officeid)
  });

  $('input[name="alloffice"]').click(function(){
  
    if($('input[name="alloffice"]').prop("checked") === true){
      $('input[name="officeid[]"]').each(function(){
        officeid.push($(this).val())
      })
    }
    else if($('input[name="alloffice"]').prop("checked") === false){
      $('input[name="officeid[]"]').each(function(){
        for( var i = 0; i < officeid.length; i++){ 
          if ( officeid[i] === $(this).val()) { 
            officeid.splice(i, 1); 
          }
        }
      })
    }
    //console.log(officeid)
      officewisedepartment(officeid)
  });
});

function officewisedepartment(officeid){
  var userid=$('.userid').val();
  $.ajax({
      type:'get',
      url:'{!!URL::to('officewisedepartments')!!}',
      data:{'officeid':officeid,'userid':userid},
      success:function(data){
        console.log(data)
        var departmentdata='<ul><li><input type="checkbox" name="alldepartment"><label>Checked All</label><ul style="list-style-type: none;">';
        for (var i = 0; i < data.length; i++) {
          if(data[i].status == 'checked'){
            departmentdata += '<li class="departmentid" id="department"><input type="checkbox" value="'+data[i].departmentid+'" class="departmentid {{ $errors->has("departmentid") ? " is-invalid" : " " }}" id="departmentid" name="departmentid[]" checked=""> <label>'+data[i].departmentname+'</label></li>'
          }
          else
          {
            departmentdata += '<li class="departmentid" id="department"><input type="checkbox" value="'+data[i].departmentid+'" {{ (is_array(old("departmentid")) and in_array( '+data[i].departmentid+', old("departmentid"))) ? " checked" : " " }} class="departmentid {{ $errors->has("departmentid") ? " is-invalid" : " " }}" id="departmentid" name="departmentid[]"> <label>'+data[i].departmentname+'</label></li>'
          }
        }
          departmentdata += '</ul></li></ul>';
        $('.departmentdata').html(departmentdata)
        
        if($('input[name="departmentid[]"]').prop("checked") == true){
          $('input[name="alldepartment"]').prop("checked",'checked')
        }
        //console.log(departmentdata)
      },
      error:function(){
      //console.log('error');
      }
      });
}
</script>
@endsection