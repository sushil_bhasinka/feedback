@extends('layouts.main')
@section('title', '| User Infromation')
@section('stylesheet')
<style type="text/css">
</style>
@endsection
@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Users</h3>
		
			<div class="box-tools pull-right">
				<a href='userinfo/create'  role="button" class=' btn btn-info btn-sm'><span class='text-white'>Add New User</span></a>
			</div>
		</div>
		<div class="box-body">
			{{-- <input type="hidden" name="urlvalue" value="{{'/userinfo'}}" class='activeurl'> --}}
			{{-- <div class='table-responsive' id="userinfo">	 --}}
			<table id='tbl_userinfo' class="table table-responsive table-striped table-condensed" >
				<thead class='bg-info'>
					<tr class="align-middle text-center">
						<th class="align-middle text-center" >ID</th>
						<th class="align-middle text-center" >User Name</th>
						<th class="align-middle text-center" >Full Name</th>
						<th class="align-middle text-center" >Email</th>
						<th class="align-middle text-center" >Action</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr class='align-middle text-center'>
						<td >{{$user->id}}</td>
						<td >{{$user->name}}</td>
						<td >{{$user->fullname}}</td>
						<td >{{$user->email}}</td>
						<td ><a href="/userinfo/{{$user->id}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@endsection


@section('script')

<script type="text/javascript">
$(document).ready( function () {
    $('#tbl_userinfo').dataTable({
	ordering:false,
    paginate:true
    });
} );
</script>

<script type="text/javascript">
// $(".userinfo").on("click", function() {
// 	$('.userinfo').removeClass('selected');
// 	$(this).addClass('selected');

// 	var userid = $(this).find("td").eq(0).html();

// 	$('#userpermission').attr("href",'/userpermission/'+userid);
// });
</script>

@endsection