<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

<meta charset="utf-8">
<title>Car Wash Feedback</title>
<meta name="description" content="">
<meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" >

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="{{ asset('frontend/css/base.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/vendor.css')}}">
<link rel="stylesheet" href="{{ asset('frontend/css/main.css')}}">
<link rel="stylesheet" href="{{asset('plugins/sweetalert2/sweetalert2.min.css')}}">

<script src="{{ asset('frontend/js/modernizr.js')}}"></script>
<script src="{{ asset('frontend/js/pace.min.js')}}"></script>

<link rel="icon" href="favicon.ico" type="image/x-icon">
</head>
<body id="top">

<header class="s-header">
<div class="header-logo">
<a class="site-logo" href="{{ url('/') }}">
	Car Wash Center
</a>
</div>
<nav class="header-nav">
<a href="#0" class="header-nav__close" title="close"><span>Close</span></a>
<div class="header-nav__content">
<h3>Navigation</h3>
<ul class="header-nav__list">
<li class="current"><a  href="{{ url('/') }}" >Home</a></li>
<li><a class="smoothscroll" href="#clients" title="clients">Client Feedback</a></li>
<li><a class="smoothscroll" href="#feedback" title="contact">Send Feedback</a></li>
</ul>
</div> 
</nav> 
<a class="header-menu-toggle" href="#0">
<span class="header-menu-text">Menu</span>
<span class="header-menu-icon"></span>
</a>
</header> 

<section id="home" class="s-home target-section" data-parallax="scroll" data-image-src="{{ asset('frontend/images/pexels-photo-3354648.jpeg') }}" data-natural-width=3000 data-natural-height=2000 data-position-y=center>
<div class="overlay"></div>
<div class="shadow-overlay"></div>
<div class="home-content">
<div class="row home-content__main">
<h3>Welcome to Car Wash Center</h3>
<h1>
10 years of experience <br>
in shining cars <br>
That pride you <br>
feel after a car wash
</h1>
</div>
<div class="home-content__scroll">
<a href="#clients" class="scroll-link smoothscroll">
<span>Scroll Down</span>
</a>
</div>
<div class="home-content__line"></div>
</div> 

</section> 
<!-- 
<section id='about' class="s-about">
<div class="row section-header has-bottom-sep" data-aos="fade-up">
<div class="col-full">
<h3 class="subhead subhead--dark">Hello There</h3>
<h1 class="display-1 display-1--light">We Are Glint</h1>
</div>
</div> 
<div class="row about-desc" data-aos="fade-up">
<div class="col-full">
<p>
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.
</p>
</div>
</div> 
<div class="row about-stats stats block-1-4 block-m-1-2 block-mob-full" data-aos="fade-up">
<div class="col-block stats__col ">
<div class="stats__count">127</div>
<h5>Awards Received</h5>
</div>
<div class="col-block stats__col">
<div class="stats__count">1505</div>
<h5>Cups of Coffee</h5>
</div>
<div class="col-block stats__col">
<div class="stats__count">109</div>
<h5>Projects Completed</h5>
</div>
<div class="col-block stats__col">
<div class="stats__count">102</div>
<h5>Happy Clients</h5>
</div>
</div> 
<div class="about__line"></div>
</section>  -->

<!-- <section id='services' class="s-services">
<div class="row section-header has-bottom-sep" data-aos="fade-up">
<div class="col-full">
<h3 class="subhead">What We Do</h3>
<h1 class="display-2">We’ve got everything you need to launch and grow your business</h1>
</div>
</div> 
<div class="row services-list block-1-2 block-tab-full">
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon">
<i class="icon-paint-brush"></i>
</div>
<div class="service-text">
<h3 class="h2">Brand Identity</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon">
<i class="icon-group"></i>
</div>
<div class="service-text">
<h3 class="h2">Illustration</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon">
<i class="icon-megaphone"></i>
</div>
<div class="service-text">
<h3 class="h2">Marketing</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon">
<i class="icon-earth"></i>
</div>
<div class="service-text">
<h3 class="h2">Web Design</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon">
<i class="icon-cube"></i>
</div>
<div class="service-text">
<h3 class="h2">Packaging Design</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
<div class="col-block service-item" data-aos="fade-up">
<div class="service-icon"><i class="icon-lego-block"></i></div>
<div class="service-text">
<h3 class="h2">Web Development</h3>
<p>Nemo cupiditate ab quibusdam quaerat impedit magni. Earum suscipit ipsum laudantium.
Quo delectus est. Maiores voluptas ab sit natus veritatis ut. Debitis nulla cumque veritatis.
Sunt suscipit voluptas ipsa in tempora esse soluta sint.
</p>
</div>
</div>
</div> 
</section>  -->

<section id="clients" class="s-clients">
<div class="row section-header" data-aos="fade-up">
<div class="col-full">
<h3 class="subhead">Clients Feedback</h3>
<h1 class="display-2"></h1>
</div>
</div> 
<div class="row clients-testimonials" data-aos="fade-up">
<div class="col-full">
<div class="testimonials">
	@foreach($feedbacks as $feedback)
	@if($feedback->status == "Approved")
	<div class="testimonials__slide">
	<p>{{ $feedback->comments }}</p>
	<div  class="testimonials__info">
	<span class="testimonials__name">{{ $feedback->clientname }}</span>
	</div>
	</div>
	@endif
	@endforeach
</div>
</div> 
</div> 
</section> 

<section id="feedback" class="s-contact">
<div class="overlay"></div>
<div class="contact__line"></div>
<div class="row section-header" data-aos="fade-up">
<div class="col-full">
<h3 class="subhead">Feedback</h3>
<h1 class="display-2 display-2--light">Questionairee </h1>
</div>
</div>
<div class="row contact-content" data-aos="fade-up">
<div class="col-md-12">
	@if(Session::has('success'))
<div class="alert alert-success" role="alert">
	<strong>Success:</strong> {{Session::get('success')}}
</div>
@endif
<h3 class="h6">Send Us A Feedback</h3>
<form  method="post" action="{{ url('/sendfeedback') }}" novalidate="novalidate">
	@csrf
<div class="form-field">
	<span style="font-size:22px;"><strong>1. Your Name?</strong></span>
	<input name="clientName" type="text" id="clientName" placeholder="Your Name" value="" minlength="2" required="" aria-required="true" class="full-width" autocomplete="off">
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>2. Your Gender?</strong></span><br>
	<input type="radio" value="Male" required="" aria-required="true" name="question1">Male
	<input type="radio" value="Female" required="" aria-required="true" name="question1" style="margin-left: 25px!important;">Female
	<input type="radio" value="Others" required="" aria-required="true" name="question1" style="margin-left: 25px!important;">Others
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>3. Your Age?</strong></span><br>
	<input type="radio" value="17-24" required="" aria-required="true" name="question2">17-24
	<input type="radio" value="25-32" required="" aria-required="true" name="question2" style="margin-left: 25px!important;">25-32
	<input type="radio" value="33-39" required="" aria-required="true" name="question2" style="margin-left: 25px!important;">33-39
	<input type="radio" value="40-50" required="" aria-required="true" name="question2" style="margin-left: 25px!important;">40-50
	<input type="radio" value="Above 50" required="" aria-required="true" name="question2" style="margin-left: 25px!important;">Above 50
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>4. Your Profession?</strong></span><br>
	<input type="radio" value="Job holder" required="" aria-required="true" name="question3">Job holder
	<input type="radio" value="Student" required="" aria-required="true" name="question3" style="margin-left: 25px!important;">Student    
	<input type="radio" value="Business" required="" aria-required="true" name="question3" style="margin-left: 25px!important;">Business
	<input type="radio" value="Other" required="" aria-required="true" name="question3" style="margin-left: 25px!important;">Other
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>5. How likely is it that you would recommend this company to a friend or colleague?</strong></span><br>
	<input type="radio" value="Extremely Likely" required="" aria-required="true" name="question4">Extremely Likely
	<input type="radio" value="Likely" required="" aria-required="true" name="question4" style="margin-left: 25px!important;">Likely    
	<input type="radio" value="Neutral" required="" aria-required="true" name="question4" style="margin-left: 25px!important;">Neutral
	<input type="radio" value="Not at all likely" required="" aria-required="true" name="question4" style="margin-left: 25px!important;">Not at all likely
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>6. Overall, how satisfied or dissatisfied are you with our SERVICE?</strong></span><br>
	<input type="radio" value="Very satisfied" required="" aria-required="true" name="question5">Very satisfied
	<input type="radio" value="Somewhat satisfied" required="" aria-required="true" name="question5" style="margin-left: 25px!important;">Somewhat satisfied    
	<input type="radio" value="Neither satisfied nor dissatisfied" required="" aria-required="true" name="question5" style="margin-left: 25px!important;">Neither satisfied nor dissatisfied
	<input type="radio" value="Somewhat dissatisfied" required="" aria-required="true" name="question5" style="margin-left: 25px!important;">Somewhat dissatisfied
	<input type="radio" value="Very dissatisfied" required="" aria-required="true" name="question5" style="margin-left: 25px!important;">Very dissatisfied
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>7. Which of the following words would you use to describe our SERVICE? </strong></span><br>
	<input type="radio" value="Reliable" required="" aria-required="true" name="question6">Reliable   
	<input type="radio" value="High quality" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">High quality
	<input type="radio" value="Useful" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Useful
	<input type="radio" value="Unique" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Unique
	<input type="radio" value="Good value for money" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Good value for money
	<input type="radio" value="Overpriced" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Overpriced
	<input type="radio" value="Impractical" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Impractical<br>
	<input type="radio" value="Ineffective" required="" aria-required="true" name="question6">Ineffective
	<input type="radio" value="Poor quality" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Poor quality
	<input type="radio" value="Unreliable" required="" aria-required="true" name="question6" style="margin-left: 25px!important;">Unreliable
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>8. How well do our SERVICE meet your needs? </strong></span><br>
	<input type="radio" value="Extremely well" required="" aria-required="true" name="question7">Extremely well
	<input type="radio" value="Very well" required="" aria-required="true" name="question7" style="margin-left: 25px!important;">Very well   
	<input type="radio" value="Somewhat well" required="" aria-required="true" name="question7" style="margin-left: 25px!important;">Somewhat well
	<input type="radio" value="Not so well" required="" aria-required="true" name="question7" style="margin-left: 25px!important;">Not so well
	<input type="radio" value="Not at all well" required="" aria-required="true" name="question7" style="margin-left: 25px!important;">Not at all well
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>9. How would you rate the quality of the SERVICE? </strong></span><br>
	<input type="radio" value="Very high quality" required="" aria-required="true" name="question8">Very high quality
	<input type="radio" value="High quality" required="" aria-required="true" name="question8" style="margin-left: 25px!important;">High quality  
	<input type="radio" value="Neither high nor low quality" required="" aria-required="true" name="question8" style="margin-left: 25px!important;">Neither high nor low quality
	<input type="radio" value="Low quality" required="" aria-required="true" name="question8" style="margin-left: 25px!important;">Low quality
	<input type="radio" value="Very low quality" required="" aria-required="true" name="question8" style="margin-left: 25px!important;">Very low quality
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>10. How responsive have we been to your questions or concerns about our SERVICE?</strong></span><br>
	<input type="radio" value="Extremely responsive" required="" aria-required="true" name="question9">Extremely responsive
	<input type="radio" value="Very responsive" required="" aria-required="true" name="question9" style="margin-left: 25px!important;">Very responsive
	<input type="radio" value="Somewhat responsive" required="" aria-required="true" name="question9" style="margin-left: 25px!important;">Somewhat responsive
	<input type="radio" value="Not so responsive" required="" aria-required="true" name="question9" style="margin-left: 25px!important;">Not so responsive
	<input type="radio" value="Not at all responsive" required="" aria-required="true" name="question9" style="margin-left: 25px!important;">Not at all responsive<br>
	<input type="radio" value="Not applicable" required="" aria-required="true" name="question9">Not applicable
</div>
<div class="form-field">
	<span style="font-size:22px;"><strong>11. Do you have any other comments, questions, or concerns?</strong></span>
<textarea name="comments" id="contactMessage" placeholder="Your Comments" rows="10" cols="120" required="" aria-required="true" class=""></textarea>
	
</div>
<div class="form-field">
<input type="submit" name="" value="Submit" class="btn--primary form-submit">
</div>
</form>

</div> 
<!-- <div class="contact-secondary">
<div class="contact-info">
<h3 class="h6 hide-on-fullwidth">Contact Info</h3>
<div class="cinfo">
<h5>Where to Find Us</h5>
<p>
1600 Amphitheatre Parkway<br>
Mountain View, CA<br>
94043 US
</p>
</div>
<div class="cinfo">
<h5>Call Us At</h5>
<p>
Phone: (+63) 555 1212<br>
Mobile: (+63) 555 0100<br>
Fax: (+63) 555 0101
</p>
</div>
</div> 
</div>  -->
</div> 
</section> 

<footer>
<div class="row footer-bottom">
<div class="col-twelve">
<div class="copyright">
<span>© Copyright 2020</span>
</div>
<div class="go-top">
<a class="smoothscroll" title="Back to Top" href="#top"><i class="icon-arrow-up" aria-hidden="true"></i></a>
</div>
</div>
</div> 
</footer> 

<div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
<div class="pswp__bg"></div>
<div class="pswp__scroll-wrap">
<div class="pswp__container">
<div class="pswp__item"></div>
<div class="pswp__item"></div>
<div class="pswp__item"></div>
</div>
<div class="pswp__ui pswp__ui--hidden">
<div class="pswp__top-bar">
<div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button> <button class="pswp__button pswp__button--share" title="Share"></button> <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button> <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
<div class="pswp__preloader">
<div class="pswp__preloader__icn">
<div class="pswp__preloader__cut">
<div class="pswp__preloader__donut"></div>
</div>
</div>
</div>
</div>
<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
<div class="pswp__share-tooltip"></div>
</div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button> <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
<div class="pswp__caption">
<div class="pswp__caption__center"></div>
</div>
</div>
</div>
</div> 

<div id="preloader">
<div id="loader">
<div class="line-scale-pulse-out">
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
</div>
</div>
</div>
<script src="{{ asset('frontend/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ asset('frontend/js/plugins.js')}}"></script>
<script src="{{ asset('frontend/js/main.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
	 var URL = window.location.href;
        var newURL = URL.replace (/^[a-z]{4}\:\/{2}[a-z]{1,}\:[0-9]{1,4}.(.*)/, '$1');
        var base_url = window.location.origin;
        if(URL == base_url+"/#feedback_success"){
          const Toast = Swal.mixin({
			  toast: true,
			  position: 'top-end',
			  showConfirmButton: false,
			  timer: 6000,
			  timerProgressBar: true,
			  didOpen: (toast) => {
			    toast.addEventListener('mouseenter', Swal.stopTimer)
			    toast.addEventListener('mouseleave', Swal.resumeTimer)
			  }
			})

			Toast.fire({
			  icon: 'success',
			  title: 'Your Feedback Submitted Successfully'
			})
        }
</script>
</body>
</html>