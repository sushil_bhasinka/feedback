@extends('layouts.main')
@section('title', '|Module Entry')

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Modules</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" name="urlvalue" value="{{'/modules/create'}}" class='activeurl'>
					{!! Form::open(array( 'route'=>'modules.store','data-parsley-validate' => '', 'files'=>'true')) !!}
					<div class="form-group">

						{{ Form::hidden('moduleid', getMaxId('module', 'moduleid')) }}
						{{Form::label('modulename', 'Module:')}}
						{{Form::text('modulename', null, array('class'=>'form-control modulename' .($errors->has('modulename') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Module'))}}
						@if($errors->has('modulename'))
						<span class="invalid-feedback">
							<strong>{{ 'Duplicate Module' }}</strong>
						</span>
						@endif
						<div id='moduleerror'></div>
						{{Form::label('moduleorder', 'Module Order:')}}
						{{Form::text('moduleorder', null, array('class'=>'form-control' .($errors->has('moduleorder') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Module Order'))}}
						@if($errors->has('moduleorder'))
						<span class="invalid-feedback">
							<strong>{{ 'Duplicate Module Order' }}</strong>
						</span>
						@endif
						<label for="usertypeid">User Type</label>
						<select class="form-control{{ $errors->has('usertypeid') ? ' is-invalid' : '' }}" id="usertypeid"
							name="usertypeid">
							<option disabled selected>Select User Type</option>
							@foreach($usertype as $type)
							<option value="{{ $type->usertypeid }}">{{ $type->usertype }}</option>
							@endforeach
						</select>
						@if($errors->has('usertypeid'))
						<span class="invalid-feedback">
							<strong>{{ "User Type can't be empty" }}</strong>
						</span>
						@endif
					</div>
					<input type="submit" class="btn btn-primary" value='Save' id='savemodule'>
				</div>

				<div class="col-md-6">
					<table id="tbl_modules" class="table table-striped table-hover table-sm table-condensed">
						<thead class='bg-info'>
							<tr class="text-center">
								<th class="align-middle">S.N.</th>
								<th class="align-middle">Module Name</th>
								<th class="align-middle">Module Order</th>
								<th class="align-middle">User Type</th>
								<th class="align-middle">Action</th>
							</tr>
						</thead>
						<tbody>
							@php
							$count=1;
							@endphp
							@foreach($allmodules as $module)
							<tr class='align-middle text-left'>
								<td>{{$count}}</td>
								<td>{{$module->modulename}}</td>
								<td>{{$module->moduleorder}}</td>
								<td>{{$module->usertypeid}}</td>
								<td ><a href="/modules/{{$module->moduleid}}/edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
								@php
								$count++;
								@endphp
							</tr>
							@endforeach
		
						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>

@endsection


@section('script')

<script type="text/javascript">
	$(document).ready( function () {
		 $('#tbl_modules').dataTable({
		ordering:false,
		 paginate:true
		 });
	} );
</script>

<script type="text/javascript">
	$(document).ready(function(){
$(document).on('keyup','.modulename',function(){
// console.log("hmm its change");
var modulename=$(this).val();
//console.log(modulename);
modulename=modulename.toUpperCase();
console.log(modulename);
$.ajax({
type:'get',
url:'{!!URL::to('uniquemodulename')!!}',
data:{'modulename':modulename},
success:function(data){
//console.log('success');
console.log(data);
//console.log(data.length);
if(data.data1.length !=0)
{

document.getElementById('moduleerror').innerHTML='<p class="text-danger"><strong>Duplicate Module</strong> </p>';
$("#savemodule").prop("disabled",true);
// }
}
else
{
document.getElementById('moduleerror').innerHTML='';
$("#savemodule").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});

</script>
@endsection