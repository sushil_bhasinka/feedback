@extends('layouts.main')
@section('title', '|Edit Module')

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Edit Module</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" name="urlvalue" value="{{'/modules/create'}}" class='activeurl'>
					{!!Form::model($module, ['route'=>['modules.update', $module->moduleid], 'method'=>'PUT',
					'files'=>'true', 'data-parsley-validate' => ''])!!}
					<div class="form-group">
						{{Form::hidden('moduleid', $module->moduleid, ['class'=>'moduleid'])}}
						{{Form::label('modulename', 'Module:')}}
						{{Form::text('modulename', null, array('class'=>'form-control modulename' .($errors->has('modulename') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Module'))}}
						@if($errors->has('modulename'))
						<span class="invalid-feedback">
							<strong>{{ 'Duplicate Module' }}</strong>
						</span>
						@endif
						<div id='moduleerror'></div>
						{{Form::label('moduleorder', 'Module Order:')}}
						{{Form::text('moduleorder', null, array('class'=>'form-control' .($errors->has('moduleorder') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Module Order'))}}
						@if($errors->has('moduleorder'))
						<span class="invalid-feedback">
							<strong>{{ 'Duplicate Module Order' }}</strong>
						</span>
						@endif
						<label for="usertypeid">User Type</label>
						<select class="form-control{{ $errors->has('usertypeid') ? ' is-invalid' : '' }}" id="usertypeid"
							name="usertypeid">
							<option disabled selected>Select User Type</option>
							@foreach($usertype as $type)
							@if($type->usertypeid == $module->usertypeid)
							<option value="{{ $type->usertypeid }}" selected>{{ $type->usertype }}</option>
							@else
							<option value="{{ $type->usertypeid }}">{{ $type->usertype }}</option>
							@endif
							@endforeach
						</select>
						@if($errors->has('usertypeid'))
						<span class="invalid-feedback">
							<strong>{{ "User Type can't be empty" }}</strong>
						</span>
						@endif
					</div>

					<div class="card-footer">
						<input type="submit" class="btn btn-primary" value='Save' id='savemodule'>
						<a href="{{'/modules/create'}}"><button type="button"
								class="btn btn-outline-primary float-right">Back</button></a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('script')
	{{-- unique validation of modulename --}}
	<script type="text/javascript">
		$(document).ready(function(){
$(document).on('keyup','.modulename',function(){
console.log("hmm its change");
var modulename=$(this).val();
var moduleid=$('.moduleid').val();
//console.log(modulename);
modulename=modulename.toUpperCase();
console.log(modulename);
$.ajax({
type:'get',
url:'{!!URL::to('uniquemodulename')!!}',
data:{'modulename':modulename, 'moduleid':moduleid},
success:function(data){
//console.log('success');
console.log(data);
//console.log(data.length);
if(data.data1.length !=0)
{
if(data.uniquemodule.length==0)
{
document.getElementById('moduleerror').innerHTML='<p class="text-danger"><strong>Duplicate Module</strong> </p>';
$("#savemodule").prop("disabled",true);
}
else
{
document.getElementById('moduleerror').innerHTML='';
$("#savemodule").prop("disabled",false);
}


// }
}
else
{
document.getElementById('moduleerror').innerHTML='';
$("#savemodule").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});

	</script>
	@endsection