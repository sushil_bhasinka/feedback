@extends('layouts.main')
@section('title', '|Edit Sub Module')

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Edit Sub Module</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-6">
					<input type="hidden" name="urlvalue" value="{{'/submodules/create'}}" class='activeurl'>
      {!!Form::model($submodule, ['route'=>['submodules.update', $submodule->submoduleid], 'method'=>'PUT', 'files'=>'true', 'data-parsley-validate' => ''])!!}
      <div class="card-body">
        <div class="form-group">
          {{form::hidden('submoduleid', $submodule->submoduleid, ['class'=>'submoduleid'])}}
          
          
          {{Form::label('moduleid', 'Module:')}}
          <select class="form-control moduleid" name='moduleid'>
            @foreach($modules as $module)
            @if($submodule->moduleid==$module->moduleid)
            <option value='{{$module->moduleid}}' selected>{{$module->modulename}}</option>
            @else
            <option value='{{$module->moduleid}}' >{{$module->modulename}}</option>
            @endif
            @endforeach
            
          </select>
          {{Form::label('submodulename', 'Sub Module:')}}
          {{Form::text('submodulename', null, array('class'=>'form-control submodulename' .($errors->has('submodulename') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Sub Module'))}}
          @if($errors->has('submodulename'))
          <span class = "invalid-feedback">
            <strong>{{ 'Duplicate Sub Module' }}</strong>
          </span>
          @endif
          <div id='submoduleerror'></div>
          {{Form::label('submoduleorder', 'Sub Module Order:')}}
          {{Form::text('submoduleorder', null, array('class'=>'form-control' .($errors->has('submoduleorder') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Sub Module Order'))}}
          @if($errors->has('submoduleorder'))
          <span class = "invalid-feedback">
            <strong>{{ 'Duplicate Sub Module Order' }}</strong>
          </span>
          @endif
          {{Form::label('url', 'URL')}}
          {{--  {{Form::('url', null, array('class'=>'form-control' .($errors->has('url') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter URL'))}} --}}
          @if($submodule->url !=null)
          <input type="" value="{{$submodule->url}}" name="url" class='form-control', required="true", placeholder="Enter URL">
          @else
          <input type="" name="url" class='form-control', required="true", placeholder="Enter URL">
          @endif
          @if($errors->has('url'))
          <span class = "invalid-feedback">
            <strong>{{ 'Duplicate URL' }}</strong>
          </span>
          @endif

          {{-- {{Form::label('mainurlid', 'Main Url:')}}
          <select class="form-control mainurlid" name='mainurlid'>
            @foreach($mainurls as $mainurl)
            @if($submodule->mainurlid==$mainurl->mainurlid)
            <option value='{{$mainurl->mainurlid}}' selected>{{$mainurl->mainurl}}</option>
            @else
            <option value='{{$mainurl->mainurlid}}' >{{$mainurl->mainurl}}</option>
            @endif
            @endforeach
            
          </select> --}}
          
        </div>
        
        
        
        
        <!-- /.card-body -->
        <div class="card-footer">
          <input type="submit" class="btn btn-primary" value='Save' id='savesubmodule'>
          <a href="{{'/submodules/create'}}"><button type="button" class="btn btn-outline-primary float-right">Back</button></a>
        </div>
      </div>
      {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('content')
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Sub Module</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      
    </div>
    <!-- /.card -->
  </div>
</div>

@endsection
@section('script')
{{-- unique validation for submoulename --}}
<script type="text/javascript">
$(document).ready(function(){
$(document).on('keyup','.submodulename',function(){
console.log("hmm its change");
var submodulename=$(this).val();
var moduleid=$('.moduleid').val();
var submoduleid=$('.submoduleid').val();
//console.log(submodulename);
submodulename=submodulename.toUpperCase();
console.log(submodulename);
$.ajax({
type:'get',
url:'{!!URL::to('uniquesubmodulename')!!}',
data:{'submodulename':submodulename, 'moduleid':moduleid, 'submoduleid':submoduleid},
success:function(data){
//console.log('success');
console.log(data);
//console.log(data.length);
if(data.data1.length !=0)
{
if(data.uniquesubmodule.length==0)
{
document.getElementById('submoduleerror').innerHTML='<p class="text-danger"><strong>Duplicate Submodule</strong> </p>';
$("#savesubmodule").prop("disabled",true);
}
else
{
document.getElementById('submoduleerror').innerHTML='';
$("#savesubmodule").prop("disabled",false);
}

}
else
{
document.getElementById('submoduleerror').innerHTML='';
$("#savesubmodule").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});
</script>
@endsection