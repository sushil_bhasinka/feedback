@extends('layouts.main')
@section('title', '|Sub Module Entry')

@section('content')
<section class="content">
	<div class="box box-default">
		<div class="box-header">
			<h3 class="box-title">Sub Modules</h3>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-5">
					<input type="hidden" name="urlvalue" value="{{'/submodules/create'}}" class='activeurl'>
					{!! Form::open(array( 'route'=>'submodules.store','data-parsley-validate' => '', 'files'=>'true')) !!}
					<div class="card-body">
						<div class="form-group">

							{{ Form::hidden('submoduleid', getMaxId('submodule', 'submoduleid')) }}
							{{-- <input type="hidden" name="academicyearid" value={{getMaxId('academicyear', 'academicyearid')}}>
							--}}
							{{Form::label('moduleid', 'Module:')}}
							{!! Form::select('moduleid', $mod, null, ['class' => 'form-control moduleid', 'placeholder' =>
							'Select Module', 'id'=>'moduleid']) !!}
							{{Form::label('submodulename', 'Sub Module:')}}
							{{Form::text('submodulename', null, array('class'=>'form-control submodulename' .($errors->has('submodulename') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Sub Module'))}}
							@if($errors->has('submodulename'))
							<span class="invalid-feedback">
								<strong>{{ 'Duplicate Sub Module' }}</strong>
							</span>
							@endif
							<div id='submoduleerror'></div>
							{{Form::label('submoduleorder', 'Sub Module Order:')}}
							{{Form::text('submoduleorder', null, array('class'=>'form-control' .($errors->has('submoduleorder') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter Sub Module Order'))}}
							@if($errors->has('submoduleorder'))
							<span class="invalid-feedback">
								<strong>{{ 'Duplicate Sub Module Order' }}</strong>
							</span>
							@endif
							{{Form::label('url', 'URL')}}
							{{--  {{Form::('url', null, array('class'=>'form-control' .($errors->has('url') ? ' is-invalid' : ''), 'required'=>'', 'placeholder'=>'Enter URL'))}}
							--}}
							<input type="" name="url" class='form-control' , required="true" , placeholder="Enter URL">
							@if($errors->has('url'))
							<span class="errors">
								<strong class='text-danger'>{{ 'Duplicate URL' }}</strong>
							</span>
							@endif

							{{-- {{Form::label('mainurlid', 'Main Url:')}}
							{!! Form::select('mainurlid', $mainurlid, null, ['class' => 'form-control mainurlid', 'placeholder'
							=> 'Select Main Url', 'id'=>'mainurlid', 'required']) !!} --}}
						</div>
						<!-- /.card-body -->
						<div class="card-footer">
							<input type="submit" class="btn btn-primary" value='Save' id='savesubmodule'>
						</div>
					</div>
					{!! Form::close() !!}
				</div>

				<div class="col-md-7">
					<table id="tbl_submodules" class="table table-striped table-hover table-sm table-condensed">
						<thead class='bg-info'>
							<tr class="text-center">
								<th class="align-middle">S.N.</th>
								<th class="align-middle">Module Name</th>
								<th class="align-middle">Sub Module Name</th>
								<th class="align-middle">Sub Module Order</th>
								<th class="align-middle">URL</th>
								<th class="align-middle">Action</th>

								{{-- <th class="align-middle" >Main URL</th> --}}
							</tr>
						</thead>
						<tbody>
							@php
							$count=1;
							@endphp
							@foreach($sub as $submodule)
							<tr class='align-middle text-center'>
								<td>{{$count}}</td>
								<td>{{$submodule->modulename}}</td>
								<td>{{$submodule->submodulename}}</td>
								<td>{{$submodule->submoduleorder}}</td>
								<td>{{$submodule->url}}</td>
								<td><a href="/submodules/{{$submodule->submoduleid}}/edit"><i class="fa fa-pencil"
											aria-hidden="true"></i></a></td>
								@php
								$count++;
								@endphp
							</tr>

							@endforeach

						</tbody>
					</table>
				</div>
			</div>
		</div>
</section>

@endsection

@section('script')

<script type="text/javascript">
	$(document).ready( function () {
      $('#tbl_submodules').dataTable({
    ordering:false,
      paginate:true
      });
  } );
</script>


{{--show submodule based on module --}}
<script type="text/javascript">
	$(document).ready(function(){
$(document).on('change','.moduleid',function(){
// console.log("hmm its change");
var moduleid=$(this).val();
//console.log(courseid);
var div=$(this).parent();

$.ajax({
type:'get',
url:'{!!URL::to('findsubmodules')!!}',
data:{'moduleid':moduleid},
success:function(data){
// console.log('success');
//console.log(data);
//console.log(data.length);
var $table = $('<table class="table table-hover table-striped table-sm"></table>');
$('#submoduletable').html('');
$table.append('<thead class="bg-info text-center"><th> S.N. </th><th> Sub Module </th><th>Sub Module Order</th><th>URL</th></thead>');
$table.append('<tbody class="text-center">');
for(var i=0;i<data.length;i++)
{
var count=i+1;
$table.append('<tr link='+data[i].submoduleid+'/edit><td class= "w-25" >'+count+'</td> <td>'+data[i].submodulename+'</td><td>'+data[i].submoduleorder+'</td><td>'+data[i].url+'</td></tr>');
$(document).on("dblclick","tr[link]",function() // start (open edit page by double clicking)
{
window.location.href=$(this).attr('link');
}); //end
$table.append('</tr>');
$('#submoduletable').append($table);
}
$table.append('</tbody>')

$('#submoduletable').append($table);
},


error:function(){
//console.log('error');
}
});
});
})
</script>
{{-- unique validation of submodulename --}}
<script type="text/javascript">
	$(document).ready(function(){
$(document).on('keyup','.submodulename',function(){
console.log("hmm its change");
var submodulename=$(this).val();
var moduleid=$('.moduleid').val();
//console.log(submodulename);
submodulename=submodulename.toUpperCase();
console.log(submodulename);
$.ajax({
type:'get',
url:'{!!URL::to('uniquesubmodulename')!!}',
data:{'submodulename':submodulename, 'moduleid':moduleid},
success:function(data){
//console.log('success');
console.log(data);
//console.log(data.length);
if(data.data1.length !=0)
{

document.getElementById('submoduleerror').innerHTML='<p class="text-danger"><strong>Duplicate Sub Module</strong> </p>';
$("#savesubmodule").prop("disabled",true);



// }
}
else
{
document.getElementById('submoduleerror').innerHTML='';
$("#savesubmodule").prop("disabled",false);
}
},
error:function(){
//console.log('error');
}
});
});
});


</script>
@endsection