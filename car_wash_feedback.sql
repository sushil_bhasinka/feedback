-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2020 at 03:44 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `car_wash_feedback`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesslevel`
--

CREATE TABLE `accesslevel` (
  `accesslevelid` bigint(20) NOT NULL,
  `accesslevelname` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `accesslevel`
--

INSERT INTO `accesslevel` (`accesslevelid`, `accesslevelname`) VALUES
(1, 'Global Admin'),
(2, 'Admin'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `accessleveluser`
--

CREATE TABLE `accessleveluser` (
  `id` bigint(20) DEFAULT NULL,
  `accesslevelid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `feedbackid` bigint(20) NOT NULL,
  `question1` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question2` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question3` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question4` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question5` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question6` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question7` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question8` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `question9` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `clientname` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comments` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `moduleid` bigint(20) NOT NULL,
  `modulename` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `moduleorder` int(11) DEFAULT NULL,
  `moduleicon` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `usertypeid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`moduleid`, `modulename`, `moduleorder`, `moduleicon`, `usertypeid`) VALUES
(1, 'General Setup', 1, 'fa fa-cog', 4),
(2, 'Feedback', 2, NULL, 4),
(3, 'testsadasf', 3, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `moduleuser`
--

CREATE TABLE `moduleuser` (
  `id` bigint(20) DEFAULT NULL,
  `moduleid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `moduleuser`
--

INSERT INTO `moduleuser` (`id`, `moduleid`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `submodule`
--

CREATE TABLE `submodule` (
  `submoduleid` bigint(20) NOT NULL,
  `moduleid` int(11) DEFAULT NULL,
  `submodulename` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `submoduleorder` int(11) DEFAULT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `submodule`
--

INSERT INTO `submodule` (`submoduleid`, `moduleid`, `submodulename`, `submoduleorder`, `url`) VALUES
(1, 1, 'User Info', 1, '/userinfo'),
(2, 1, 'Module', 2, '/modules/create'),
(3, 1, 'Sub Module', 3, '/submodules/create'),
(4, 1, 'Access Level', 4, '/accesslevels/create'),
(5, 2, 'Feedback Approval', 1, '/feedback');

-- --------------------------------------------------------

--
-- Table structure for table `submoduleuser`
--

CREATE TABLE `submoduleuser` (
  `id` bigint(20) DEFAULT NULL,
  `submoduleid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `submoduleuser`
--

INSERT INTO `submoduleuser` (`id`, `submoduleid`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT '0000-00-00 00:00:00.000000' ON UPDATE CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NULL DEFAULT '0000-00-00 00:00:00.000000',
  `syncstatus` varchar(3) COLLATE utf8mb4_unicode_520_ci DEFAULT 'N',
  `fullname` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `contact` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `photo` longblob,
  `phototype` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `isactive` varchar(2) COLLATE utf8mb4_unicode_520_ci DEFAULT 'N',
  `usertypeid` int(11) DEFAULT NULL,
  `posted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `syncstatus`, `fullname`, `contact`, `phone`, `photo`, `phototype`, `isactive`, `usertypeid`, `posted_by`) VALUES
(1, 'Demo', 'demo@gmail.com', '$2y$10$L4WJVnbhLn8pi8owhvPLiuP88K9IBFoLbsKUD3mYzc/MCDUNnhASy', NULL, '2020-12-10 15:17:56.672115', '2020-12-10 09:31:03.709366', 'N', 'Feedback System', '0123456789', '1234567890', NULL, NULL, 'Y', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `usertypeid` bigint(20) NOT NULL,
  `usertype` varchar(20) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`usertypeid`, `usertype`) VALUES
(4, 'ALL_USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesslevel`
--
ALTER TABLE `accesslevel`
  ADD PRIMARY KEY (`accesslevelid`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`feedbackid`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`moduleid`);

--
-- Indexes for table `submodule`
--
ALTER TABLE `submodule`
  ADD PRIMARY KEY (`submoduleid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`usertypeid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `feedbackid` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
